# SASE

This repo contains demo setup for a SASE ZTNA solution using open source components.

Gartner defines the ZTNA as follows:
"Zero trust network access (ZTNA) is a product or service that creates an identity- and context-based, logical access boundary around an application or set of applications. The applications are hidden from discovery, and access is restricted via a trust broker to a set of named entities. The broker verifies the identity, context and policy adherence of the specified participants before allowing access and prohibits lateral movement elsewhere in the network. This removes application assets from public visibility and significantly reduces the surface area for attack"

In this demo following open source components are used to realize ZTNA:

- ISTIO/Envoy + OAUTH2-proxy for reverse proxy, OIDC based authentication, finer granular access controls, L7 traffic routing, L7 load balancing

- Keycloak as a broker for integrating with various types of Employer and Partner authentication systems.

- Keycloak as IdP server too to support inbuilt IAM system

- Strongswan for terminating IPSEC clients and overlay tunnels from connectors.

- Cert-Manager for certificate management

- K8s KNCC and KNRC for exposing configuration as K8s CRs.

- MetalLB (BGP based)

- PowerDNS as Authoratitive DNS Server for customers

## Getting started

[Demo Setup](https://gitlab.com/project-emco/ecosystem/sase/-/blob/main/scripts/README.md)



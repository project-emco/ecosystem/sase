# Steps to install SASE network


## 1.Update the ingress proxy with SDEWAN CNF as sidecar container

Here, we deliver a single yaml file to install ingress-proxy gateway with SDEWAN CNF as the second container. Also we need pre-deploy the secret, certifate and so on for this depolyment. All configuration files are under the folder `./config/ingress-gateway`, we cloud deploy these using the following commands.

```bash
kubectl create namespace istio-ingress
kubectl label namespace istio-ingress istio-injection=enabled

kubectl apply -f ./config/ingress-gateway/config.yaml -n istio-ingress
kubectl apply -f ./config/ingress-gateway/gateway-sidecar.yaml -n istio-ingress
```

## 2.Install CNF helm chart and CRD helm chart

We should install CNF and CRD in both vpn gateway and data center. Please locate to `./helm` and apply the following commands:

```bash
kubectl create namespace sdewan-system
kubectl apply -f ./cert/

helm package sdewan_cnf
helm install ./cnf-0.1.0.tgz --generate-name

helm package sdewan_controllers
helm install ./controllers-0.1.0.tgz --generate-name
```


## 3.Apply the GRE Tunnel CR to deploy gre tunnel between ingress-proxy and vpn gateway(sdewan cnf)

Then, we could apply the GRE Tunnel CR and the SDEWAN crd controller will create GRE tunnel between ingress-proxy pod and SDEWAN CNF pod. Please locate to the `./config` folder and edit the field in `gre.yaml` with your configuration, you should configure the svcSubnet field with the DC service subnet which you want to export to the users to access.

```bash
# Please firstly ensure that you have load the gre module on host
# modprobe ip_gre
# modprobe nf_conntrack_proto_gre
kubectl apply -f ./gre/gre.yaml
```

## 4.Apply the IPSEC Tunnel between vpn-gateway and vpc

Before we apply the IPSEC CR to establish IPSEC tunnel between vpn gatewat and DC. We should check the CNF LB service and keep the external IP address which metalLB assigned to the LB service in mind. This IP should be configured to `ipsec_client.yaml` remote filed <cnf_external_ip>.

```bash
# On vpn gateway side
kubectl apply -f ./ipsec/ipsecproposal.yaml
# Before apply the yaml, please change the `<svc_subnet>` field, which is service subnet in your DC for users to access. `<table_id>` is the table id in your GRE CR configuration. And edit the `<vti_if_name>` as the interface name you want to set (different from others).
kubectl apply -f ./ipsec/ipsec_server.yaml

# On DC side
kubectl apply -f ./ipsec/ipsecproposal.yaml
# Before apply the yaml, please change the `<vpn_gateway_pod_subnet>` field to the right value. `<table_id>` is the table id in your GRE CR configuration. And edit the `<vti_if_name>` as the interface name you want to set (different from others).
kubectl apply -f ./ipsec/ipsec_client.yaml
```

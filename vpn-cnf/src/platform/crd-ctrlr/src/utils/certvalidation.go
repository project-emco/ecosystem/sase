// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation
package utils

import (
	"context"
	"encoding/base64"
	"log"
	"reflect"
	"strings"

	pkgerrors "github.com/pkg/errors"
	certv1 "k8s.io/api/certificates/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sdewan.akraino.org/sdewan/openwrt"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	defaultKeyType   = "rsa:2048"
	signerPrefix     = "issuers.cert-manager.io/sdewan-system."
	defaultNamespace = "default"
	issuerSuffix     = "-issuer"
)

var CertificateNotReadyErr = pkgerrors.New("Get certificate failed")
var NoErr = pkgerrors.New("No specific error")

func CheckCertificateAvailable(r client.Client, clientInfo *openwrt.OpenwrtClientInfo, cnfName, privateKeyId, certSubject string) (string, bool, error) {
	params := strings.Split(privateKeyId, " ")
	id := strings.Split(params[0], ":")
	csr := certv1.CertificateSigningRequest{}
	err := r.Get(context.Background(), client.ObjectKey{
		Namespace: defaultNamespace,
		Name:      cnfName + "-" + id[1],
	}, &csr)
	if client.IgnoreNotFound(err) != nil {
		return "", false, err
	}

	if reflect.ValueOf(csr).IsZero() {
		errC := make(chan error, 1)
		var genError error

		log.Println("Generating CSR for", id[1], "with key params", params[1])
		go generateCSR(r, id[1], cnfName, certSubject, params[1], clientInfo, errC)

		genError = <-errC
		close(errC)
		if !pkgerrors.Is(genError, CertificateNotReadyErr) && !pkgerrors.Is(genError, NoErr) {
			return "", false, genError
		} else {
			return "", false, CertificateNotReadyErr
		}
	}

	if string(csr.Status.Certificate) == "" {
		log.Println("Waiting for certificate")
		return "", false, CertificateNotReadyErr
	}

	localPublicCert := strings.SplitAfter(string(csr.Status.Certificate), "-----END CERTIFICATE-----")[0]
	return base64.StdEncoding.EncodeToString([]byte(localPublicCert)), true, nil
}

func generateCSR(r client.Client, privateKeyId, ownerName, certSubject, keyParams string, clientInfo *openwrt.OpenwrtClientInfo, ch chan error) {
	//TODO: call certificate api to get keyLabel, CSR
	req := openwrt.PKCS11CertReq{
		Cert: openwrt.CertInfo{
			Subject: certSubject,
			KeyPair: openwrt.KeyParams{
				KeyType: keyParams,
				Label:   "label-" + privateKeyId,
				ID:      privateKeyId,
			},
		},
	}

	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	certClient := openwrt.CertClient{OpenwrtClient: openwrtClient}
	resp, err := certClient.CreateCSR(req)
	if err != nil {
		log.Println("Failed to create CSR")
		ch <- err
		return
	}

	csrRequest, err := base64.StdEncoding.DecodeString(resp)
	if err != nil {
		log.Println("Failed to decode csr", err)
		ch <- err
		return
	}

	// convert x509 certificaterequest into k8s csr
	k8sCSR := &certv1.CertificateSigningRequest{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ownerName + "-" + privateKeyId,
			Namespace: defaultNamespace,
		},
		Spec: certv1.CertificateSigningRequestSpec{
			Request:    csrRequest,
			SignerName: signerPrefix + ownerName + issuerSuffix,
			Usages:     []certv1.KeyUsage{certv1.UsageClientAuth},
		},
	}

	err = r.Create(context.Background(), k8sCSR)
	if err != nil {
		log.Println("Error in creating kubernetes CSR", err)
		ch <- err
		return
	}

	ch <- NoErr
}

// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// OpenvpnSpec defines the desired state of Openvpn
type OpenvpnSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Name      string `json:"name,omitempty"`
	Dev       string `json:"dev,omitempty"`
	Proto     string `json:"proto,omitempty"`
	Remote    string `json:"remote,omitempty"`
	Ca        string `json:"ca,omitempty"`
	Cert      string `json:"cert,omitempty"`
	Key       string `json:"key,omitempty"`
	Tls_crypt string `json:"tls_crypt,omitempty"`
}

// OpenvpnStatus defines the observed state of Openvpn
type OpenvpnStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Openvpn is the Schema for the openvpns API
type Openvpn struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   OpenvpnSpec  `json:"spec,omitempty"`
	Status SdewanStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// OpenvpnList contains a list of Openvpn
type OpenvpnList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Openvpn `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Openvpn{}, &OpenvpnList{})
}

// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type greServer struct {
	PodSelector *metav1.LabelSelector `json:"podSelector,omitempty"`
	Namespace   string                `json:"namespace,omitempty"`
	TunnelIP    string                `json:"tunnelIP,omitempty"`
	TableName   string                `json:"tableName,omitempty"`
	SvcSubnet   string                `json:"svcSubnet,omitempty"`
	//PodIP       string `json:"-"`
}

func (in *greServer) DeepCopyInto(out *greServer) {
	*out = *in

}

// GREtunnelSpec defines the desired state of GREtunnel
type GREtunnelSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of GREtunnel. Edit gretunnel_types.go to remove/update
	//TunnelName  string    `json:"tunnelName,omitempty"`
	TunnelName  string    `json:"tunnelName,omitempty"`
	RightServer greServer `json:"rightServer,omitempty"`
	LeftServer  greServer `json:"leftServer,omitempty"`
}

// GREtunnelStatus defines the observed state of GREtunnel
type GREtunnelStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// +optional
	LeftIP string `json:"leftIP,omitempty"`
	// +optional
	RightIP string `json:"rightIP,omitempty"`
	// +optional
	Message string `json:"message,omitempty"`
}

func (c *GREtunnelStatus) IsEqual(s *GREtunnelStatus) bool {
	if c.LeftIP != s.LeftIP ||
		c.RightIP != s.RightIP {
		return false
	}

	return true
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// GREtunnel is the Schema for the gretunnels API
type GREtunnel struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GREtunnelSpec   `json:"spec,omitempty"`
	Status GREtunnelStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// GREtunnelList contains a list of GREtunnel
type GREtunnelList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []GREtunnel `json:"items"`
}

func init() {
	SchemeBuilder.Register(&GREtunnel{}, &GREtunnelList{})
}

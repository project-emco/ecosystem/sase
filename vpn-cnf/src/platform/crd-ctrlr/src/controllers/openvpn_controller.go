// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation

package controllers

import (
	"context"
	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"reflect"
	batchv1alpha1 "sdewan.akraino.org/sdewan/api/v1alpha1"
	"sdewan.akraino.org/sdewan/openwrt"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/source"
	//"sigs.k8s.io/controller-runtime/pkg/log"
)

// OpenvpnReconciler reconciles a Openvpn object
type OpenvpnReconciler struct {
	Log logr.Logger
	client.Client
	Scheme *runtime.Scheme
}

var vpnHandler = new(OpenvpnHandler)

type OpenvpnHandler struct {
}

func (m *OpenvpnHandler) GetType() string {
	return "Openvpn"
}

func (m *OpenvpnHandler) GetName(instance client.Object) string {
	openvpn := instance.(*batchv1alpha1.Openvpn)
	return openvpn.Name
}

func (m *OpenvpnHandler) GetFinalizer() string {
	return "Openvpn.finalizers.sdewan.akraino.org"
}

func (m *OpenvpnHandler) GetInstance(r client.Client, ctx context.Context, req ctrl.Request) (client.Object, error) {
	instance := &batchv1alpha1.Openvpn{}
	err := r.Get(ctx, req.NamespacedName, instance)
	return instance, err
}

func (m *OpenvpnHandler) Convert(instance client.Object, deployment appsv1.Deployment) (openwrt.IOpenWrtObject, error) {
	openvpn := instance.(*batchv1alpha1.Openvpn)
	openvpn.Spec.Name = openvpn.ObjectMeta.Name
	openvpnObject := openwrt.SdewanOpenvpn(openvpn.Spec)
	return &openvpnObject, nil
}

func (m *OpenvpnHandler) IsEqual(instance1 openwrt.IOpenWrtObject, instance2 openwrt.IOpenWrtObject) bool {
	openvpn1 := instance1.(*openwrt.SdewanOpenvpn)
	openvpn2 := instance2.(*openwrt.SdewanOpenvpn)
	return reflect.DeepEqual(*openvpn1, *openvpn2)
}

func (m *OpenvpnHandler) GetObject(clientInfo *openwrt.OpenwrtClientInfo, name string) (openwrt.IOpenWrtObject, error) {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	openvpnClient := openwrt.OpenvpnClient{OpenwrtClient: openwrtClient}
	ret, err := openvpnClient.GetOpenvpn(name)
	return ret, err
}

func (m *OpenvpnHandler) CreateObject(clientInfo *openwrt.OpenwrtClientInfo, instance openwrt.IOpenWrtObject) (openwrt.IOpenWrtObject, error) {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	openvpnClient := openwrt.OpenvpnClient{OpenwrtClient: openwrtClient}
	openvpn := instance.(*openwrt.SdewanOpenvpn)
	return openvpnClient.CreateOpenvpn(*openvpn)
}

func (m *OpenvpnHandler) DeleteObject(clientInfo *openwrt.OpenwrtClientInfo, name string) error {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	openvpnClient := openwrt.OpenvpnClient{OpenwrtClient: openwrtClient}
	return openvpnClient.DeleteOpenvpn(name)
}
func (m *OpenvpnHandler) Restart(clientInfo *openwrt.OpenwrtClientInfo) (bool, error) {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	service := openwrt.ServiceClient{OpenwrtClient: openwrtClient}
	return service.ExecuteService("openvpn", "restart")
}

func (m *OpenvpnHandler) UpdateObject(clientInfo *openwrt.OpenwrtClientInfo, instance openwrt.IOpenWrtObject) (openwrt.IOpenWrtObject, error) {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	openvpnClient := openwrt.OpenvpnClient{OpenwrtClient: openwrtClient}
	openvpn := instance.(*openwrt.SdewanOpenvpn)
	return openvpnClient.UpdateOpenvpn(*openvpn)
}

//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=openvpns,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=openvpns/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=openvpns/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Openvpn object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.12.2/pkg/reconcile
func (r *OpenvpnReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	return ProcessReconcile(r.Client, r.Log, ctx, req, vpnHandler)
}

// SetupWithManager sets up the controller with the Manager.
func (r *OpenvpnReconciler) SetupWithManager(mgr ctrl.Manager) error {
	ps := builder.WithPredicates(predicate.GenerationChangedPredicate{})
	return ctrl.NewControllerManagedBy(mgr).
		For(&batchv1alpha1.Openvpn{}, ps).
		Watches(
			&source.Kind{Type: &appsv1.Deployment{}},
			handler.EnqueueRequestsFromMapFunc(GetToRequestsFunc(r.Client, &batchv1alpha1.OpenvpnList{})),
			Filter).
		Complete(r)
}

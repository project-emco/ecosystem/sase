// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation

package controllers

import (
	"context"
	"reflect"
	"time"

	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/runtime"
	batchv1alpha1 "sdewan.akraino.org/sdewan/api/v1alpha1"
	"sdewan.akraino.org/sdewan/openwrt"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

// GREserverReconciler reconciles a GREserver object
type GREserverReconciler struct {
	Log           logr.Logger
	CheckInterval time.Duration
	client.Client
	Scheme *runtime.Scheme
}

var serverHandler = new(GREserverHandler)

type GREserverHandler struct {
}

func (m *GREserverHandler) GetType() string {
	return "GREserver"
}
func (m *GREserverHandler) GetName(instance client.Object) string {
	greserver := instance.(*batchv1alpha1.GREserver)
	return greserver.Spec.Name
}

func (m *GREserverHandler) GetFinalizer() string {
	return "GREserver.finalizers.sdewan.akraino.org"
}

func (m *GREserverHandler) GetInstance(r client.Client, ctx context.Context, req ctrl.Request) (client.Object, error) {
	instance := &batchv1alpha1.GREserver{}
	err := r.Get(ctx, req.NamespacedName, instance)
	return instance, err
}

func (m *GREserverHandler) Convert(instance client.Object, deployment appsv1.Deployment) (openwrt.IOpenWrtObject, error) {
	greserver := instance.(*batchv1alpha1.GREserver)
	openvpnObject := openwrt.SdewanGREserver(greserver.Spec)
	return &openvpnObject, nil
}

func (m *GREserverHandler) IsEqual(instance1 openwrt.IOpenWrtObject, instance2 openwrt.IOpenWrtObject) bool {
	gretunnel1 := instance1.(*openwrt.SdewanGREserver)
	gretunnel2 := instance2.(*openwrt.SdewanGREserver)
	return reflect.DeepEqual(*gretunnel1, *gretunnel2)
}

func (m *GREserverHandler) GetObject(clientInfo *openwrt.OpenwrtClientInfo, name string) (openwrt.IOpenWrtObject, error) {

	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	greserverClient := openwrt.GREserverClient{OpenwrtClient: openwrtClient}
	ret, err := greserverClient.GREserver(name)
	return ret, err
}

func (m *GREserverHandler) CreateObject(clientInfo *openwrt.OpenwrtClientInfo, instance openwrt.IOpenWrtObject) (openwrt.IOpenWrtObject, error) {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	greserverClient := openwrt.GREserverClient{OpenwrtClient: openwrtClient}
	greserver := instance.(*openwrt.SdewanGREserver)
	return greserverClient.CreateGREserver(greserver)
}

func (m *GREserverHandler) DeleteObject(clientInfo *openwrt.OpenwrtClientInfo, name string) error {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	greserverClient := openwrt.GREserverClient{OpenwrtClient: openwrtClient}
	return greserverClient.DeleteOpenvpn(name)
}
func (m *GREserverHandler) Restart(clientInfo *openwrt.OpenwrtClientInfo) (bool, error) {
	return true, nil
}

func (m *GREserverHandler) UpdateObject(clientInfo *openwrt.OpenwrtClientInfo, instance openwrt.IOpenWrtObject) (openwrt.IOpenWrtObject, error) {
	openwrtClient := openwrt.GetOpenwrtClient(*clientInfo)
	greserverClient := openwrt.GREserverClient{OpenwrtClient: openwrtClient}
	greserver := instance.(*openwrt.SdewanGREserver)
	return greserverClient.UpdateOpenvpn(greserver)
}

//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=greservers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=greservers/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=greservers/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the GREserver object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.12.2/pkg/reconcile
func (r *GREserverReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	// TODO(user): your logic here
	return ProcessReconcile(r.Client, r.Log, ctx, req, serverHandler)

}

// SetupWithManager sets up the controller with the Manager.
func (r *GREserverReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&batchv1alpha1.GREserver{}).
		Complete(r)
}

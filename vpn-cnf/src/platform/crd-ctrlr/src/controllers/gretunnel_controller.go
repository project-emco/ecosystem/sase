// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation
package controllers

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	errs "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	batchv1alpha1 "sdewan.akraino.org/sdewan/api/v1alpha1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// GREtunnelReconciler reconciles a GREtunnel object
type GREtunnelReconciler struct {
	client.Client
	Log           logr.Logger
	Scheme        *runtime.Scheme
	mux           sync.Mutex
	CheckInterval time.Duration
}

var greQuery = false

//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=gretunnels,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=gretunnels/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=batch.sdewan.akraino.org,resources=gretunnels/finalizers,verbs=update

func (r *GREtunnelReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("gretunnel", req.NamespacedName)
	during, _ := time.ParseDuration("5s")
	instance, err := r.GetInstance(req)
	if err != nil {
		if errs.IsNotFound(err) {
			// No instance
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return ctrl.Result{RequeueAfter: during}, nil
	}
	finalizerName := "gretunnel.finalizers.sdewan.akraino.org"
	delete_timestamp := getDeletionTempstamp(instance)

	if delete_timestamp.IsZero() {
		// Creating or updating CR
		// Process instance
		err = r.processInstance(instance)
		if err != nil {
			log.Error(err, "Adding/Updating CR")
			instance.Status.Message = err.Error()
			r.Status().Update(ctx, instance)
			return ctrl.Result{}, err
		}

		finalizers := getFinalizers(instance)
		if !containsString(finalizers, finalizerName) {
			appendFinalizer(instance, finalizerName)
			if err := r.Update(ctx, instance); err != nil {
				return ctrl.Result{}, err
			}
			log.Info("Added finalizer for gretunnel")
		}
	} else {
		// Deleting CR
		// Remove instance
		err = r.removeInstance(instance)
		if err != nil {
			log.Error(err, "Deleting CR")
			return ctrl.Result{RequeueAfter: during}, nil
		}

		finalizers := getFinalizers(instance)
		if containsString(finalizers, finalizerName) {
			removeFinalizer(instance, finalizerName)
			if err := r.Update(ctx, instance); err != nil {
				return ctrl.Result{}, err
			}
		}
	}

	return ctrl.Result{}, nil
}

func (r *GREtunnelReconciler) GetInstance(req ctrl.Request) (*batchv1alpha1.GREtunnel, error) {
	instance := &batchv1alpha1.GREtunnel{}
	err := r.Get(context.Background(), req.NamespacedName, instance)
	return instance, err
}

func (r *GREtunnelReconciler) processInstance(instance *batchv1alpha1.GREtunnel) error {
	r.mux.Lock()
	defer r.mux.Unlock()
	// get  left pod ip
	ns := instance.Spec.LeftServer.Namespace
	ps := instance.Spec.LeftServer.PodSelector.MatchLabels
	leftIP, err1 := r.getIP4s(ns, ps)
	if err1 != nil || len(leftIP) == 0 {
		if err1 != nil {
			r.removeResources(instance)
			r.Log.Error(err1, "GREtunnel")
		}
		return errors.New("cannot get left pod IP")
	}
	// get  right pod ip
	ns = instance.Spec.RightServer.Namespace
	ps = instance.Spec.RightServer.PodSelector.MatchLabels
	rightIP, err2 := r.getIP4s(ns, ps)
	if err2 != nil || len(rightIP) == 0 {
		if err2 != nil {
			r.removeResources(instance)
			r.Log.Error(err2, "GREtunnel")
		}
		return errors.New("cannot get right pod IP")
	}

	var curStatus = batchv1alpha1.GREtunnelStatus{
		LeftIP:  leftIP,
		RightIP: rightIP,
	}

	if !curStatus.IsEqual(&instance.Status) {
		r.removeResources(instance)
		r.addResources(instance, &curStatus)
		instance.Status = curStatus
		r.Status().Update(context.Background(), instance)

	}

	return nil
}

func (r *GREtunnelReconciler) getIP4s(ns string, ps map[string]string) (string, error) {
	podList := &corev1.PodList{}
	err := r.List(context.Background(), podList, client.MatchingLabels(ps), client.InNamespace(ns))
	if err != nil || len(podList.Items) == 0 {
		return "", err
	}

	return podList.Items[0].Status.PodIP, err
}

func (r *GREtunnelReconciler) removeResources(instance *batchv1alpha1.GREtunnel) error {
	if instance.Status.LeftIP != "" {
		liftserver_name := instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName
		liftserver := &batchv1alpha1.GREserver{
			ObjectMeta: metav1.ObjectMeta{
				Name:      liftserver_name,
				Namespace: instance.Spec.LeftServer.Namespace,
				Labels:    instance.Spec.LeftServer.PodSelector.MatchLabels,
			},
			Spec: batchv1alpha1.GREserverSpec{},
		}
		err := r.Delete(context.Background(), liftserver)
		if err != nil {
			r.Log.Error(err, "Deleting NAT CR : "+liftserver_name)
		}

	}
	if instance.Status.RightIP != "" {
		rightserver_name := instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName
		rightserver := &batchv1alpha1.GREserver{
			ObjectMeta: metav1.ObjectMeta{
				Name:      rightserver_name,
				Namespace: instance.Spec.RightServer.Namespace,
				Labels:    instance.Spec.RightServer.PodSelector.MatchLabels,
			},
			Spec: batchv1alpha1.GREserverSpec{},
		}
		err := r.Delete(context.Background(), rightserver)
		if err != nil {
			r.Log.Error(err, "Deleting NAT CR : "+rightserver_name)
		}

	}

	// check resource
	err := wait.PollImmediate(time.Second, time.Second*10,
		func() (bool, error) {
			server_temp := &batchv1alpha1.GREserver{}
			leftserver_get := r.Get(context.Background(), client.ObjectKey{
				Namespace: instance.Spec.LeftServer.Namespace,
				Name:      instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName,
			}, server_temp)
			if errs.IsNotFound(leftserver_get) {
				return true, nil
			}
			r.Log.Info("Waiting for Deleting left server : " + instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName)

			return false, nil
		},
	)

	if err != nil {
		r.Log.Error(err, "Failed to delete left server CR : "+instance.ObjectMeta.Name+"-"+instance.Spec.TunnelName)
	}

	err = wait.PollImmediate(time.Second, time.Second*10,
		func() (bool, error) {
			server_temp := &batchv1alpha1.GREserver{}
			rightserver_get := r.Get(context.Background(), client.ObjectKey{
				Namespace: instance.Spec.RightServer.Namespace,
				Name:      instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName,
			}, server_temp)
			if errs.IsNotFound(rightserver_get) {
				return true, nil
			}
			r.Log.Info("Waiting for Deleting right server : " + instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName)

			return false, nil
		},
	)

	if err != nil {
		r.Log.Error(err, "Failed to delete right server CR : "+instance.ObjectMeta.Name+"-"+instance.Spec.TunnelName)
	}

	return nil
}

func (r *GREtunnelReconciler) addResources(instance *batchv1alpha1.GREtunnel, status *batchv1alpha1.GREtunnelStatus) error {

	liftserver_name := instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName
	liftserver := &batchv1alpha1.GREserver{
		ObjectMeta: metav1.ObjectMeta{
			Name:      liftserver_name,
			Namespace: instance.Spec.LeftServer.Namespace,
			Labels:    instance.Spec.LeftServer.PodSelector.MatchLabels,
		},
		Spec: batchv1alpha1.GREserverSpec{
			Name:      instance.Spec.TunnelName,
			LocalIP:   status.LeftIP,
			RemoteIP:  status.RightIP,
			TableName: instance.Spec.LeftServer.TableName,
			SvcSubnet: "",
		},
	}

	err := r.Create(context.Background(), liftserver)
	if err != nil {
		r.Log.Error(err, "Creating left GREserver : "+liftserver_name)
	}

	rightserver_name := instance.ObjectMeta.Name + "-" + instance.Spec.TunnelName
	rightserver := &batchv1alpha1.GREserver{
		ObjectMeta: metav1.ObjectMeta{
			Name:      rightserver_name,
			Namespace: instance.Spec.RightServer.Namespace,
			Labels:    instance.Spec.RightServer.PodSelector.MatchLabels,
		},
		Spec: batchv1alpha1.GREserverSpec{
			Name:      instance.Spec.TunnelName,
			LocalIP:   status.RightIP,
			RemoteIP:  status.LeftIP,
			TableName: "",
			SvcSubnet: instance.Spec.RightServer.SvcSubnet,
		},
	}

	err = r.Create(context.Background(), rightserver)
	if err != nil {
		r.Log.Error(err, "Creating left GREserver : "+rightserver_name)
	}
	return nil
}

func (r *GREtunnelReconciler) removeInstance(instance *batchv1alpha1.GREtunnel) error {
	r.mux.Lock()
	defer r.mux.Unlock()
	return r.removeResources(instance)
}

func (r *GREtunnelReconciler) check() {

	gretunnel_list := &batchv1alpha1.GREtunnelList{}
	err := r.List(context.Background(), gretunnel_list)
	if err != nil {
		r.Log.Error(err, "Failed to list GREtunnel CRs")
	} else {
		if len(gretunnel_list.Items) > 0 {
			for _, inst := range gretunnel_list.Items {
				r.processInstance(&inst)
			}
		}
	}
}

func (r *GREtunnelReconciler) SafeCheck() {
	doCheck := true
	r.mux.Lock()
	if !greQuery {
		greQuery = true
	} else {
		doCheck = false
	}
	r.mux.Unlock()

	if doCheck {

		r.check()
		r.mux.Lock()
		greQuery = false
		r.mux.Unlock()
	}
}

func (r *GREtunnelReconciler) SetupWithManager(mgr ctrl.Manager) error {
	go func() {
		interval := time.After(r.CheckInterval)
		for {
			select {
			case <-interval:

				r.SafeCheck()
				interval = time.After(r.CheckInterval)
			case <-context.Background().Done():
				return
			}
		}
	}()
	return ctrl.NewControllerManagedBy(mgr).
		For(&batchv1alpha1.GREtunnel{}).
		Complete(r)
}

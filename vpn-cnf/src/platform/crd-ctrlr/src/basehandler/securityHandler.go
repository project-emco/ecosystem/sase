// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2022 Intel Corporation

package basehandler

import (
	appsv1 "k8s.io/api/apps/v1"
	"sdewan.akraino.org/sdewan/openwrt"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type SecurityHandler interface {
	CertificateValidation(r client.Client, instance openwrt.IOpenWrtObject, clientInfo *openwrt.OpenwrtClientInfo, deployment appsv1.Deployment) (openwrt.IOpenWrtObject, error)
}

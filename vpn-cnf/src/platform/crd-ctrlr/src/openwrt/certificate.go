// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2022 Intel Corporation

package openwrt

import (
	"encoding/json"
)

const (
	pkcsBaseURL = "sdewan/pkcs11/"
	version     = "v1/"
)

type CertClient struct {
	OpenwrtClient *openwrtClient
}

// Certificate Info
type PKCS11CertReq struct {
	Cert CertInfo `json:"cert"`
}

type CertInfo struct {
	Pem     string    `json:"pem"`
	Subject string    `json:"subject"`
	KeyPair KeyParams `json:"key_pair"`
}

type KeyParams struct {
	KeyType string `json:"key_type"`
	Label   string `json:"label"`
	ID      string `json:"id"`
}

// Certificate APIs
// request certificatesigningrequest
func (c *CertClient) CreateCSR(request PKCS11CertReq) (string, error) {
	cert_req_obj, _ := json.Marshal(request)
	response, err := c.OpenwrtClient.Post(pkcsBaseURL+version+"csr", string(cert_req_obj))
	if err != nil {
		return "", err
	}

	return response, nil
}

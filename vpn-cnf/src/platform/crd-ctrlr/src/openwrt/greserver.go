// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation

package openwrt

import (
	"encoding/json"
)

const (
	GREtunnelBaseURL = "sdewan/greserver/v1/"
)

type GREserverClient struct {
	OpenwrtClient *openwrtClient
}

// Openvpn
type SdewanGREserver struct {
	Name      string `json:"name,omitempty"`
	LocalIP   string `json:"localIP,omitempty"`
	RemoteIP  string `json:"remoteIP,omitempty"`
	TableName string `json:"tableName,omitempty"`
	SvcSubnet string `json:"svcSubnet,omitempty"`
}

func (o *SdewanGREserver) GetName() string {
	return o.Name
}

func (o *SdewanGREserver) SetFullName(namespace string) {
	o.Name = namespace + o.Name
}

type SdewanGREservers struct {
	GREtunnels []SdewanGREserver `json:"GREservers"`
}

// GREserver APIs
// get GREservers
func (m *GREserverClient) GREservers() (*SdewanGREservers, error) {
	var response string
	var err error
	response, err = m.OpenwrtClient.Get(GREtunnelBaseURL + "greservers")
	if err != nil {
		return nil, err
	}

	var sdewanGREservers SdewanGREservers
	err = json.Unmarshal([]byte(response), &sdewanGREservers)
	if err != nil {
		return nil, err
	}

	return &sdewanGREservers, nil
}

// get GREserver
func (m *GREserverClient) GREserver(name string) (*SdewanGREserver, error) {
	var response string
	var err error
	response, err = m.OpenwrtClient.Get(GREtunnelBaseURL + "greservers/" + name)
	if err != nil {
		return nil, err
	}
	var sdewanGREserver SdewanGREserver
	err = json.Unmarshal([]byte(response), &sdewanGREserver)
	if err != nil {
		return nil, err
	}

	return &sdewanGREserver, nil
}

// create Openvpn
func (m *GREserverClient) CreateGREserver(greserverConf *SdewanGREserver) (*SdewanGREserver, error) {
	var response string
	var err error
	greserver_obj, _ := json.Marshal(greserverConf)
	response, err = m.OpenwrtClient.Post(GREtunnelBaseURL+"greservers", string(greserver_obj))
	if err != nil {
		return nil, err
	}

	var sdewanGREserver SdewanGREserver
	err = json.Unmarshal([]byte(response), &sdewanGREserver)
	if err != nil {
		return nil, err
	}
	return &sdewanGREserver, nil
}

// delete Openvpn
func (m *GREserverClient) DeleteOpenvpn(name string) error {
	_, err := m.OpenwrtClient.Delete(GREtunnelBaseURL + "greservers/" + name)
	if err != nil {
		return err
	}
	return nil
}

// update Openvpn
func (m *GREserverClient) UpdateOpenvpn(greserverConf *SdewanGREserver) (*SdewanGREserver, error) {
	var response string
	var err error
	gretunnel_obj, _ := json.Marshal(greserverConf)
	gretunnel_name := greserverConf.Name
	response, err = m.OpenwrtClient.Put(GREtunnelBaseURL+"greserver/"+gretunnel_name, string(gretunnel_obj))
	if err != nil {
		return nil, err
	}
	var sdewanGREserver SdewanGREserver
	err = json.Unmarshal([]byte(response), &sdewanGREserver)
	if err != nil {
		return nil, err
	}
	return &sdewanGREserver, nil
}

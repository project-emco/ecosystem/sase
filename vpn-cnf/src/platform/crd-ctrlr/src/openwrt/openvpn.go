// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2021 Intel Corporation

package openwrt

import (
	"encoding/json"
)

const (
	OpenvpnBaseURL = "sdewan/openvpn/v1/"
)

type OpenvpnClient struct {
	OpenwrtClient *openwrtClient
}

// Openvpn
type SdewanOpenvpn struct {
	Name      string `json:"name"`
	Dev       string `json:"dev"`
	Proto     string `json:"proto"`
	Remote    string `json:"remote"`
	Ca        string `json:"ca"`
	Cert      string `json:"cert"`
	Key       string `json:"key"`
	Tls_crypt string `json:"tls_crypt"`
}

func (o *SdewanOpenvpn) GetName() string {
	return o.Name
}

func (o *SdewanOpenvpn) SetFullName(namespace string) {
	o.Name = namespace + o.Name
}

type SdewanOpenvpns struct {
	Openvpns []SdewanOpenvpn `json:"openvpns"`
}

// Openvpn APIs
// get Openvpns
func (m *OpenvpnClient) GetOpenvpns() (*SdewanOpenvpns, error) {
	var response string
	var err error
	response, err = m.OpenwrtClient.Get(OpenvpnBaseURL + "openvpn")
	if err != nil {
		return nil, err
	}

	var sdewanOpenvpns SdewanOpenvpns
	err = json.Unmarshal([]byte(response), &sdewanOpenvpns)
	if err != nil {
		return nil, err
	}

	return &sdewanOpenvpns, nil
}

// get openvpn
func (m *OpenvpnClient) GetOpenvpn(name string) (*SdewanOpenvpn, error) {
	var response string
	var err error
	response, err = m.OpenwrtClient.Get(OpenvpnBaseURL + "openvpns/" + name)
	if err != nil {
		return nil, err
	}
	var sdewanOpenvpn SdewanOpenvpn
	err = json.Unmarshal([]byte(response), &sdewanOpenvpn)
	if err != nil {
		return nil, err
	}

	return &sdewanOpenvpn, nil
}

// create Openvpn
func (m *OpenvpnClient) CreateOpenvpn(openvpnConf SdewanOpenvpn) (*SdewanOpenvpn, error) {
	var response string
	var err error
	openvpn_obj, _ := json.Marshal(openvpnConf)
	response, err = m.OpenwrtClient.Post(OpenvpnBaseURL+"openvpns", string(openvpn_obj))
	if err != nil {
		return nil, err
	}

	var sdewanOpenvpn SdewanOpenvpn
	err = json.Unmarshal([]byte(response), &sdewanOpenvpn)
	if err != nil {
		return nil, err
	}
	return &sdewanOpenvpn, nil
}

// delete Openvpn
func (m *OpenvpnClient) DeleteOpenvpn(name string) error {
	_, err := m.OpenwrtClient.Delete(OpenvpnBaseURL + "openvpns/" + name)
	if err != nil {
		return err
	}
	return nil
}

// update Openvpn
func (m *OpenvpnClient) UpdateOpenvpn(openvpnConf SdewanOpenvpn) (*SdewanOpenvpn, error) {
	var response string
	var err error
	openvpn_obj, _ := json.Marshal(openvpnConf)
	openvpn_name := openvpnConf.Name
	response, err = m.OpenwrtClient.Put(OpenvpnBaseURL+"openvpns/"+openvpn_name, string(openvpn_obj))
	if err != nil {
		return nil, err
	}
	var sdewanOpenvpn SdewanOpenvpn
	err = json.Unmarshal([]byte(response), &sdewanOpenvpn)
	if err != nil {
		return nil, err
	}
	return &sdewanOpenvpn, nil
}

package main

import (
	"log"

	"github.com/gin-gonic/gin"

	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/api"
	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/client"
	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/config"
	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/middleware"
)

func main() {
	var err error
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("start sdewan hsm server")

	config.GetConfFromENV()

	client.PKCS11Cli = client.DefaultPKCS11Client()
	err = client.InitDefaultToken()
	if err != nil {
		panic(err)
	}

	r := gin.Default()
	r.Use(middleware.CheckReqSource)
	api.Router(r)
	err = r.Run(":8081")
	if err != nil {
		return
	}
}

package client

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
)

func Exec(cmdStr string, out io.Writer, args ...string) error {
	cmd := exec.Command(cmdStr, args...)
	cmd.Stdout = out
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	return err
}

func ExecDaemon(cmdStr string, out io.Writer, args ...string) error {
	var err error
	cmd := exec.Command(cmdStr, args...)
	cmd.Stdout = out
	cmd.Stderr = os.Stderr

	go func(cmd *exec.Cmd) {
		err = cmd.Start()
		log.Printf("p11-kit server started")
		if err != nil {
			log.Printf("command: %v start failed: %v ", cmd.Path, err)
		}

		err = cmd.Wait()
		if err != nil {
			log.Printf("command: %v run failed: %v", cmd.Path, err)
		}
	}(cmd)
	return err
}

func CreateFileWithContent(name, path, content string) error {
	p := fmt.Sprintf("%s/%s", path, name)
	_, err := os.Stat(p)
	var f *os.File
	if os.IsNotExist(err) {
		f, err = os.Create(p)
		if err != nil {
			return err
		}
		defer func(f *os.File) {
			_ = f.Close()
		}(f)
	}
	_, err = f.WriteString(content)
	return err
}

func DeleteFile(name, path string) error {
	p := fmt.Sprintf("%v/%v", path, name)
	err := os.Remove(p)
	return err
}

func AppendFileWithContent(name, path, content string) error {
	p := fmt.Sprintf("%s/%s", path, name)
	f, err := os.Open(p)
	if err != nil {
		return err
	}
	defer func(f *os.File) {
		_ = f.Close()
	}(f)

	_, err = f.WriteString("\n" + content)
	if err != nil {
		return err
	}

	err = f.Sync()
	return err
}

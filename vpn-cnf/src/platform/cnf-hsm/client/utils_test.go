package client

import (
	"os"
	"testing"
)

func TestExec(t *testing.T) {
	cmdStr := "ls"
	err := Exec(cmdStr, os.Stdout, "-al")
	if err != nil {
		t.Fatal("exec failed")
	}
	t.Logf("test success")
}

func TestExecDaemon(t *testing.T) {
	cmdStr := "sleep"
	err := ExecDaemon(cmdStr, os.Stdout, "10")
	if err != nil {
		t.Fatal("exec failed")
	}
	t.Logf("test success")
}

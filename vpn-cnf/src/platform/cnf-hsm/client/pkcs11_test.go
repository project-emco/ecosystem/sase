package client

import (
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"testing"

	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/model"
)

func TestPKCS11InitToken(t *testing.T) {

	p := DefaultPKCS11Client()
	token := model.Token{
		Label: "sgx-1",
		Slot:  0,
		SoPin: "12345678",
		Pin:   "12345678",
	}
	err := p.PKCS11InitToken(&token)

	if err != nil {
		t.Fatal("token init failed: ", err)
	}
}

func TestPKCS11KeyPairCreate(t *testing.T) {

	p := DefaultPKCS11Client()
	token := model.Token{
		Label: "sgx-1",
		Slot:  0,
		SoPin: "12345678",
		Pin:   "12345678",
	}

	keyPair := model.KeyPair{
		KeyType: "rsa:2048",
		Label:   "cert-key",
		Id:      "0001",
	}

	err := p.PKCS11KeyPairCreate(&token, &keyPair)
	if err != nil {
		t.Fatal("create key pair failed: ", err)
	}
}

func TestPKCS11CSRCreate(t *testing.T) {

	p := DefaultPKCS11Client()
	token := model.Token{
		Label: "sgx-1",
		Slot:  0,
		SoPin: "12345678",
		Pin:   "12345678",
	}

	keyPair := &model.KeyPair{
		KeyType: "rsa:2048",
		Label:   "cert-key",
		Id:      "0001",
	}

	cert := model.Cert{
		KeyPair: keyPair,
		Subject: "/CN=node-1",
	}

	csr, err := p.PKCS11CSRCreate(&token, &cert)
	if err != nil {
		t.Fatal("create csr failed: ", err)
	}
	t.Log(csr)
}

func TestPKCS11AddCert(t *testing.T) {
	p := DefaultPKCS11Client()
	token := model.Token{
		Label: "sgx-1",
		Slot:  0,
		SoPin: "12345678",
		Pin:   "12345678",
	}

	keyPair := &model.KeyPair{
		KeyType: "rsa:2048",
		Label:   "cert-key",
		Id:      "0001",
	}

	cert := model.Cert{
		KeyPair: keyPair,
		Subject: "/CN=node-1",
		Pem:     "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN2akNDQWFZQ0FRRXdEUVlKS29aSWh2Y05BUUVMQlFBd09qRUxNQWtHQTFVRUJoTUNRMGd4RXpBUkJnTlYKQkFvVENuTjBjbTl1WjFOM1lXNHhGakFVQmdOVkJBTVREWE4wY205dVoxTjNZVzRnUTBFd0hoY05Nakl3TXpFdwpNRGd5TmpFNFdoY05Nak13TXpFd01EZ3lOakU0V2pBUU1RNHdEQVlEVlFRRERBVnpaM2d0TVRDQ0FTSXdEUVlKCktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQU14YThtYm1PbmFucVNhcjBpVFVnb0xJZERzUkxJbjEKdDlOejVjb1BhVE9YQW00YWtTQU40bEQyNVZoY0x3bkw4NzBKU0MwU0lFcHFrQzFUcDJjbE0rRGxxQVd1eGV4VQpRWFVZa21DRkR0TnVBQU80eG5IZXV6SzdDaUErdll1WGdEZ3NRK01uQU5lYS9idHZUaXVjemtjT0V6VVluK0NnCjhBQ1B4RksrZ1piaWh4bUlkRjhMTTNFaUVhRUc2aDh2ano0a01hS1F6bFU0cWEzeFJLMjJGYXFXZ2JJdlREUm8KZk9BVjE2UjNPa21Vc2hnQkVjOXJvN3lNaVkwYjV2N1IyUDJ2VkRWSTMxNS85SlNNWjk5RFpzZldoZlVXK1E4aAo1R2VaeFhlcHNTaWt0MXduQUMvUUVzSDhEREtWOFFSaFZmT0ZVTi9kODFyV2RjQjF4T1g4VDJNQ0F3RUFBVEFOCkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQVhUbkVVai9ZQjZ6RThLUkJPS0FueUVaSVplT093OExPL1NVeGlUTzYKNTh2QjVsUTFWeXlvOWhRVWZ6NDVIV01mKzIxcFd5RDVCREJEL1ZEaTFubWFXU0JXd0lNcVVhYlgrNnlEcEVKMQo0cWhSU0R4TytmNDZJL0NCZ0ZVR2tROVJmNzNOVGxiOHczQzV0NU8wWWdpSHRPck9HSXMxNUxKRDFieFh0WnhFCmkvcDlZYWR5WUV5aFNJVGxxem93ZCtEaUg3OTVJWFhNbTd2M2t2REdRak5vRzBkWEVRMkJsTENDbXpuVWR3bksKTVBQZ3B5TWhoZkdtOWtOTDlxMno3N0RvQ2wrQkN3aFcxMXR3NUhjWDd4cU1oMFMzcDg1R2pGaUNhK2lxN2owYgp4SzI2dkhZYlNTQU9HWXdZYUNYeld4MW9tQmpMNUVGcTJoYTNSODkzQi9MeXFRPT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=",
	}

	err := p.PKCS11AddCert(&token, &cert)
	if err != nil {
		t.Fatal("add cert to ctk failed: ", err)
	}

}

func TestPkcs11GetTokenSerialNum(t *testing.T) {
	p := DefaultPKCS11Client()
	token := model.Token{
		Label: "sdewan-sgx",
		Slot:  0,
		SoPin: "12345678",
		Pin:   "12345678",
	}

	serialNum, err := p.pkcs11GetTokenSerialNum(&token)
	if err != nil {
		t.Fatalf("get serial num failed: %s", err)
	}
	t.Logf("token serial num is %s", serialNum)
}

func TestP11GetObjectsByLabel(t *testing.T) {
	p := DefaultPKCS11Client()
	objs, err := p.p11GetObjectsByLabel("node-2")
	if err != nil {
		t.Fatal("get obj failed")
	}
	t.Logf("get %d objs", len(objs))
}

func TestVerifyCert(t *testing.T) {

	certStr := "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN2akNDQWFZQ0FRRXdEUVlKS29aSWh2Y05BUUVMQlFBd09qRUxNQWtHQTFVRUJoTUNRMGd4RXpBUkJnTlYKQkFvVENuTjBjbTl1WjFOM1lXNHhGakFVQmdOVkJBTVREWE4wY205dVoxTjNZVzRnUTBFd0hoY05Nakl3TXpFdwpNRGd5TmpFNFdoY05Nak13TXpFd01EZ3lOakU0V2pBUU1RNHdEQVlEVlFRRERBVnpaM2d0TVRDQ0FTSXdEUVlKCktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQU14YThtYm1PbmFucVNhcjBpVFVnb0xJZERzUkxJbjEKdDlOejVjb1BhVE9YQW00YWtTQU40bEQyNVZoY0x3bkw4NzBKU0MwU0lFcHFrQzFUcDJjbE0rRGxxQVd1eGV4VQpRWFVZa21DRkR0TnVBQU80eG5IZXV6SzdDaUErdll1WGdEZ3NRK01uQU5lYS9idHZUaXVjemtjT0V6VVluK0NnCjhBQ1B4RksrZ1piaWh4bUlkRjhMTTNFaUVhRUc2aDh2ano0a01hS1F6bFU0cWEzeFJLMjJGYXFXZ2JJdlREUm8KZk9BVjE2UjNPa21Vc2hnQkVjOXJvN3lNaVkwYjV2N1IyUDJ2VkRWSTMxNS85SlNNWjk5RFpzZldoZlVXK1E4aAo1R2VaeFhlcHNTaWt0MXduQUMvUUVzSDhEREtWOFFSaFZmT0ZVTi9kODFyV2RjQjF4T1g4VDJNQ0F3RUFBVEFOCkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQVhUbkVVai9ZQjZ6RThLUkJPS0FueUVaSVplT093OExPL1NVeGlUTzYKNTh2QjVsUTFWeXlvOWhRVWZ6NDVIV01mKzIxcFd5RDVCREJEL1ZEaTFubWFXU0JXd0lNcVVhYlgrNnlEcEVKMQo0cWhSU0R4TytmNDZJL0NCZ0ZVR2tROVJmNzNOVGxiOHczQzV0NU8wWWdpSHRPck9HSXMxNUxKRDFieFh0WnhFCmkvcDlZYWR5WUV5aFNJVGxxem93ZCtEaUg3OTVJWFhNbTd2M2t2REdRak5vRzBkWEVRMkJsTENDbXpuVWR3bksKTVBQZ3B5TWhoZkdtOWtOTDlxMno3N0RvQ2wrQkN3aFcxMXR3NUhjWDd4cU1oMFMzcDg1R2pGaUNhK2lxN2owYgp4SzI2dkhZYlNTQU9HWXdZYUNYeld4MW9tQmpMNUVGcTJoYTNSODkzQi9MeXFRPT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo="

	certPem, err := base64.StdEncoding.DecodeString(certStr)
	if err != nil {
		t.Fatal(err)
	}

	block, rest := pem.Decode(certPem)
	if block == nil {
		t.Fatalf("failed to parse certificate PEM > %v > %v", block, rest)
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		t.Fatalf("failed to parse certificate: %v", err.Error())
	}

	t.Log(cert)

}

func TestPKCS11GetTokenInfo(t *testing.T) {
	p := DefaultPKCS11Client()
	token := model.Token{
		Label: "sgx-1",
		Slot:  0,
		SoPin: "12345678",
		Pin:   "12345678",
	}

	info, err := p.PKCS11GetTokenInfo(&token)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(info)
}

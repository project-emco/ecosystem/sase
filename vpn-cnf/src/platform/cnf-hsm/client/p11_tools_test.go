package client

import (
	"testing"

	"github.com/miekg/pkcs11"
)

func TestPKCS11Tool(t *testing.T) {
	p := pkcs11.New("/usr/local/lib/libp11sgx.so")
	err := p.Initialize()
	if err != nil {
		panic(err)
	}

	defer p.Destroy()
	defer p.Finalize()

	slots, err := p.GetSlotList(true)
	if err != nil {
		panic(err)
	}

	session, err := p.OpenSession(slots[0], pkcs11.CKF_SERIAL_SESSION|pkcs11.CKF_RW_SESSION)
	if err != nil {
		panic(err)
	}
	defer p.CloseSession(session)

	err = p.Login(session, pkcs11.CKU_USER, "12345678")
	if err != nil {
		panic(err)
	}
	defer p.Logout(session)

	// p.GetSlotInfo()

	template := []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_LABEL, "node-2")}
	if e := p.FindObjectsInit(session, template); e != nil {
		t.Fatalf("failed to init: %s\n", e)
		return
	}
	obj, _, e := p.FindObjects(session, 100)
	if e != nil {
		t.Fatalf("failed to find: %s\n", e)
	}

	t.Logf(">>>> total objs: %d", len(obj))
	if e := p.FindObjectsFinal(session); e != nil {
		t.Fatalf("failed to finalize: %s\n", e)
	}

	template2 := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_PUBLIC_EXPONENT, nil),
		pkcs11.NewAttribute(pkcs11.CKA_MODULUS_BITS, nil),
		pkcs11.NewAttribute(pkcs11.CKA_MODULUS, nil),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, nil),
		pkcs11.NewAttribute(pkcs11.CKA_ID, nil),
		pkcs11.NewAttribute(pkcs11.CKA_LABEL, "node-2"),
	}

	attr, err := p.GetAttributeValue(session, obj[0], template2)
	if err != nil {
		t.Fatalf("err %s\n", err)
	}
	for i, a := range attr {
		t.Logf("attr %d, type %d, value is: >>> %s <<< \n", i, a.Type, string(a.Value))
		// if a.Type == pkcs11.CKA_MODULUS {
		// 	mod := big.NewInt(0)
		// 	mod.SetBytes(a.Value)
		// 	t.Logf("modulus %s\n", mod.String())
		// }
	}

}

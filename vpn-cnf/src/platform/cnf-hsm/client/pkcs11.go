package client

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/miekg/pkcs11"

	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/config"
	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/model"
)

const CTK_MODULE = "/usr/local/lib/libp11sgx.so"

var PKCS11Cli *PKCS11Client
var DefaultToken *model.Token

type PKCS11Client struct {
	Module string

	PKCS11Tool string
	P11Kit     string
	P11Req     string
	P11Ctx     pkcs11.Ctx
}

func DefaultPKCS11Client() *PKCS11Client {
	return &PKCS11Client{
		Module:     CTK_MODULE,
		PKCS11Tool: "pkcs11-tool",
		P11Kit:     "p11-kit",
		P11Req:     "p11req",
	}
}

func InitDefaultToken() (err error) {
	DefaultToken = &model.Token{
		Label: "sdewan-sgx",
		Slot:  0,
		SoPin: config.DefaultTokenPin,
		Pin:   config.DefaultTokenPin,
	}
	err = PKCS11Cli.PKCS11InitToken(DefaultToken)
	if err != nil {
		return fmt.Errorf("init sgx token failed: %s", err)
	}

	err = PKCS11Cli.PKCS11StartP11KitServer(DefaultToken)
	if err != nil {
		return fmt.Errorf("init default p11-kit server failed: %s", err)
	}
	return
}

func (p *PKCS11Client) PKCS11GetTokenInfo(t *model.Token) (tokenInfo pkcs11.TokenInfo, err error) {
	p.P11Ctx = *pkcs11.New(CTK_MODULE)
	err = p.P11Ctx.Initialize()
	if err != nil {
		log.Printf("p11 initialize failed: err")
		return
	}

	defer p.P11Ctx.Destroy()
	defer p.P11Ctx.Finalize()

	slots, err := p.P11Ctx.GetSlotList(true)
	if err != nil {
		return
	}

	if len(slots) < 1 {
		return tokenInfo, errors.New("slot is not exist")
	}

	tokenInfo, err = p.P11Ctx.GetTokenInfo(slots[0])

	return
}

func (p *PKCS11Client) PKCS11InitToken(t *model.Token) (err error) {
	log.Println("Init Token")

	tokenInfo, err := p.PKCS11GetTokenInfo(t)
	if tokenInfo.SerialNumber != "" {
		return err
	}

	err = Exec(p.PKCS11Tool, os.Stdout,
		"--module", p.Module, "--init-token",
		"--label", t.Label,
		"--slot", strconv.Itoa(int(t.Slot)),
		"--so-pin", t.SoPin, "--init-pin", "--pin", t.Pin)
	if err != nil {
		log.Printf("pkcs11 init token failed: %s", err)
	}
	return err
}

func (p *PKCS11Client) PKCS11KeyPairIsExist(t *model.Token, k *model.KeyPair) (isExist bool, err error) {
	// Todo check by object type
	objs, err := p.p11GetObjectsByLabel(k.Label)
	if err != nil || len(objs) < 2 {
		return false, err
	}
	return true, nil
}
func (p *PKCS11Client) PKCS11CertIsExist(t *model.Token, k *model.KeyPair) (isExist bool, err error) {
	// TODO check  by object type
	objs, err := p.p11GetObjectsByLabel(k.Label)
	if err != nil || len(objs) < 3 {
		return false, err
	}
	return true, nil
}

func (p *PKCS11Client) PKCS11KeyPairCreate(t *model.Token, k *model.KeyPair) error {

	err := Exec(p.PKCS11Tool, os.Stdout,
		"--module", p.Module, "--login", "--pin", t.Pin,
		"--token", t.Label,
		"--label", k.Label, "--id", k.Id,
		"--keypairgen", "--key-type", k.KeyType,
		"--usage-sign")
	if err != nil {
		log.Printf("pkcs11 create key pair failed: %s", err)
	}
	return err
}

func (p *PKCS11Client) PKCS11KeyPairDelete(t *model.Token, k *model.KeyPair) error {
	return errors.New("can't delete key pair")
}

func (p *PKCS11Client) PKCS11CSRCreate(t *model.Token, c *model.Cert) (csr string, err error) {
	out := bytes.Buffer{}
	err = Exec(p.P11Req, &out,
		"-l", p.Module, "-i", c.KeyPair.Label,
		"-t", t.Label, "-p", t.Pin,
		"-d", c.Subject)
	if err != nil {
		log.Printf("pkcs11 generate csr failed: %s", err)
	}
	return out.String(), nil

}
func (p *PKCS11Client) PKCS11AddCert(t *model.Token, c *model.Cert) (err error) {

	derPath := fmt.Sprintf("/tmp/clientcrt_%s.der", c.KeyPair.Label)
	pemPath := fmt.Sprintf("/tmp/client_%s.crt", c.KeyPair.Label)

	pem, err := os.Create(pemPath)
	if err != nil {
		log.Fatal("create pem file failed: ", err)
		return
	}
	pem.WriteString(c.Pem)
	pem.Close()
	defer os.Remove(pemPath)

	err = Exec("openssl", os.Stdout, "x509", "-in", pemPath,
		"-outform", "DER", "-out", derPath)

	if err != nil {
		log.Fatal("transform to der failed: ", err)
		return
	}

	err = Exec(p.PKCS11Tool, os.Stdout,
		"--module", p.Module, "--login", "--pin", t.Pin,
		"--token", t.Label,
		"--id", c.KeyPair.Id, "--label", c.KeyPair.Label,
		"--type", "cert",
		"--write-object", derPath)
	if err != nil {
		log.Printf("add cert to token failed: %s", err)
	}
	return
}

func (p *PKCS11Client) PKCS11CertDelete(t *model.Token, c *model.Cert) error {

	var err error
	if c.KeyPair.Label != "" {
		err = Exec(p.PKCS11Tool, os.Stdout,
			"--module", p.Module, "--login", "--pin", t.Pin,
			"--token", t.Label,
			"--label", c.KeyPair.Label,
			"--type", "cert",
			"-b",
		)

	} else {
		err = Exec(p.PKCS11Tool, os.Stdout,
			"--module", p.Module, "--login", "--pin", t.Pin,
			"--token", t.Label,
			"--id", c.KeyPair.Id,
			"--type", "cert",
			"-b",
		)
	}
	if err != nil {
		log.Printf("pkcs11 delete cert failed: %s", err)
	}
	return err
}

func (p *PKCS11Client) PKCS11StartP11KitServer(t *model.Token) (err error) {

	t.SerialNum, err = p.pkcs11GetTokenSerialNum(t)
	if err != nil {
		return
	}
	tokenURL := fmt.Sprintf("pkcs11:model=SGXHSM%%20v2;manufacturer=SGXHSM%%20project;serial=%s;token=%s", t.SerialNum, t.Label)
	err = ExecDaemon(p.P11Kit, os.Stdout, "server", "--provider", p.Module, tokenURL)
	if err != nil {
		log.Printf("p11-kit start server failed: %s", err)
	}

	<-time.After(1 * time.Second)
	err = Exec("bash", os.Stdout, "-c", "chmod 777 /tmp/p11-kit/p11-kit-server-sgx")
	if err != nil {
		log.Printf("p11-kit change unix socket mod error: %s", err)
	}

	return
}

func (p *PKCS11Client) pkcs11GetTokenSerialNum(t *model.Token) (serialNum string, err error) {

	serialNumBuf := bytes.Buffer{}

	err = Exec("bash", &serialNumBuf, "-c", "pkcs11-tool --module /usr/local/lib/libp11sgx.so -L |awk 'NR==9{print $4}'")
	if err != nil {
		log.Printf("get serial num of token failed: %s", err)
		return
	}

	return serialNumBuf.String(), nil
}

func (p *PKCS11Client) p11GetObjectsByLabel(label string) (objHandles []pkcs11.ObjectHandle, err error) {
	p.P11Ctx = *pkcs11.New(CTK_MODULE)
	err = p.P11Ctx.Initialize()
	if err != nil {
		log.Printf("p11 initialize failed: err")
		return
	}

	defer p.P11Ctx.Destroy()
	defer p.P11Ctx.Finalize()

	slots, err := p.P11Ctx.GetSlotList(true)
	if err != nil {
		return
	}

	session, err := p.P11Ctx.OpenSession(slots[0], pkcs11.CKF_SERIAL_SESSION|pkcs11.CKF_RW_SESSION)
	if err != nil {
		return
	}
	defer p.P11Ctx.CloseSession(session)

	err = p.P11Ctx.Login(session, pkcs11.CKU_USER, config.DefaultTokenPin)
	if err != nil {
		return
	}
	defer p.P11Ctx.Logout(session)

	template := []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_LABEL, label)}
	if err = p.P11Ctx.FindObjectsInit(session, template); err != nil {
		log.Printf("failed to init: %s\n", err)
		return
	}
	objHandles, _, err = p.P11Ctx.FindObjects(session, 100)
	if err != nil {
		log.Printf("failed to find: %s\n", err)
		return
	}
	if e := p.P11Ctx.FindObjectsFinal(session); e != nil {
		log.Printf("failed to finalize: %s\n", e)
		return
	}

	return

}

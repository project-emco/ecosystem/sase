package handler

import (
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/client"
	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/model"
)

func PKCS11HealthCheck(c *gin.Context) {
	bindWithString(c, "pong\n")
}

func PKCS11CSRCreate(c *gin.Context) {

	var err error
	var req model.PKCS11Req
	if err = c.BindJSON(&req); err != nil {
		bindWithError(c, &model.InvalidArguments)
		return
	}

	if req.Token == nil {
		req.Token = client.DefaultToken
	}
	// check if the key pair exist
	isExist, err := client.PKCS11Cli.PKCS11KeyPairIsExist(req.Token, req.Cert.KeyPair)
	if !isExist {
		err = client.PKCS11Cli.PKCS11KeyPairCreate(req.Token, req.Cert.KeyPair)
		if err != nil {
			bindWithError(c, &model.KeyPairCreatedError)
			return
		}
	}

	csr, err := client.PKCS11Cli.PKCS11CSRCreate(req.Token, req.Cert)
	if err != nil {
		bindWithError(c, &model.CsrCreatedError)
		return
	}

	csrStr := base64.StdEncoding.EncodeToString([]byte(csr))
	bindWithString(c, csrStr)
}

func PKCS11AddCert(c *gin.Context) {
	var err error
	var req model.PKCS11Req
	if err = c.BindJSON(&req); err != nil {
		bindWithError(c, &model.InvalidArguments)
		return
	}

	pemByte, err := base64.StdEncoding.DecodeString(req.Cert.Pem)
	if err != nil {
		bindWithError(c, &model.Base64DecodeError)
		return
	}

	// todo check the cert is x509 format
	block, _ := pem.Decode(pemByte)
	if block == nil {
		bindWithError(c, &model.CertX509ValidateError)
		return
	}
	_, err = x509.ParseCertificate(block.Bytes)
	if err != nil {
		bindWithError(c, &model.CertX509ValidateError)
		return
	}

	req.Cert.Pem = string(pemByte)
	if req.Token == nil {
		req.Token = client.DefaultToken
	}

	isExist, err := client.PKCS11Cli.PKCS11KeyPairIsExist(req.Token, req.Cert.KeyPair)
	if !isExist || err != nil {
		bindWithError(c, &model.KeyPairNotExistError)
		return

	}

	// try to delete the old cert by label
	_ = client.PKCS11Cli.PKCS11CertDelete(req.Token, req.Cert)

	// todo delete cert if the cert is out of date
	err = client.PKCS11Cli.PKCS11AddCert(req.Token, req.Cert)
	if err != nil {
		bindWithStatusAndString(c, http.StatusInternalServerError,
			"cert add to token failed")
		return
	}
	bindWithStatusAndString(c, http.StatusOK, "success")

}

func PKCS11DeleteCert(c *gin.Context) {
	var err error
	var req model.PKCS11Req
	if err = c.BindJSON(&req); err != nil {
		bindWithError(c, &model.InvalidArguments)
		return
	}
	if req.Token == nil {
		req.Token = client.DefaultToken
	}
	// try to delete the old cert by label
	err = client.PKCS11Cli.PKCS11CertDelete(req.Token, req.Cert)
	if err != nil {
		bindWithStatusAndString(c, http.StatusInternalServerError,
			"delete cert failed")
		return
	}
	bindWithStatusAndString(c, http.StatusOK, "success")
}

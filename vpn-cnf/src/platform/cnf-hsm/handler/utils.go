package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/model"
)

func bindWithString(c *gin.Context, content string) {
	bindWithStatusAndString(c, http.StatusOK, content)
}

func bindWithError(c *gin.Context, err error) {
	switch e := err.(type) {
	case model.Error:
		bindWithStatusAndString(c, e.Status(), e.Error())
	default:
		bindWithStatusAndString(c, http.StatusInternalServerError, e.Error())
	}
}

func bindWithStatusAndString(c *gin.Context, code int, content string) {
	c.String(code, content)
}

func bindWithStatusAndJson(c *gin.Context, code int, data interface{}) {
	c.JSON(code, data)
}

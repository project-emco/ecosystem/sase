package api

import (
	"github.com/gin-gonic/gin"

	"github.com/akraino-edge-stack/icn-sdwan-hsmserver/handler"
)

func Router(r *gin.Engine) {

	pkcs11API := r.Group("/pkcs11")
	{
		pkcs11API.GET("/ping",handler.PKCS11HealthCheck)
		pkcs11API.POST("/csr", handler.PKCS11CSRCreate)
		pkcs11API.POST("/cert", handler.PKCS11AddCert)
		pkcs11API.DELETE("/cert", handler.PKCS11DeleteCert)
	}
}

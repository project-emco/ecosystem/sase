package middleware

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func CheckReqSource(c *gin.Context) {

	host := strings.Split(c.Request.Host, ":")[0]
	remoteAddr := strings.Split(c.Request.RemoteAddr, ":")[0]

	if remoteAddr == host || remoteAddr == "127.0.0.1" {
		c.Next()
		return
	}
	c.AbortWithStatus(http.StatusBadRequest)
}

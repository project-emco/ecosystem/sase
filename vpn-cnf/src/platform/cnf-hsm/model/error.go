package model

import (
	"errors"
)

type ErrorCode int

var (
	InvalidArguments      StatusError = StatusError{Code: 400, Err: errors.New("invalid arguments")}
	KeyPairNotExistError  StatusError = StatusError{Code: 440, Err: errors.New("key pair not exist")}
	KeyPairCreatedError   StatusError = StatusError{Code: 441, Err: errors.New("key pair create err")}
	CsrCreatedError       StatusError = StatusError{Code: 442, Err: errors.New("csr create err")}
	Base64DecodeError     StatusError = StatusError{Code: 443, Err: errors.New("base64 decode err")}
	CertX509ValidateError StatusError = StatusError{Code: 444, Err: errors.New("cert x509 validate err")}
)

type Error interface {
	error
	Status() int
}

type StatusError struct {
	Code int
	Err  error
}

func (se *StatusError) Error() string {
	return se.Err.Error()
}

func (se *StatusError) Status() int {
	return se.Code
}

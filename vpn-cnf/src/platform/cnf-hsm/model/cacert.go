package model

type Token struct {
	Label     string `json:"label"`
	Slot      int64  `json:"slot"`
	SoPin     string `json:"so_pin"`
	Pin       string `json:"pin"`
	SerialNum string
}

type KeyPair struct {
	KeyType string `json:"key_type"`
	Label   string `json:"label"`
	Id      string `json:"id"`
}

func NewKeyPair() *KeyPair {
	return &KeyPair{
		Id: "0001",
	}
}

type Cert struct {
	KeyPair *KeyPair `json:"key_pair"`
	Subject string   `json:"subject"`
	Pem     string   `json:"pem"`
}

type PKCS11Req struct {
	Token *Token `json:"token"`
	Cert  *Cert  `json:"cert"`
}

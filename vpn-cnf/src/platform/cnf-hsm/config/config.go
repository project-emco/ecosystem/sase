package config

import "os"

var DefaultTokenPin string = "12345678"

func GetConfFromENV() {
	ctkTokenPin := os.Getenv("CTK_TOKEN_PIN")
	if ctkTokenPin != "" {
		DefaultTokenPin = ctkTokenPin
	}
}

FROM ubuntu:focal AS builder
WORKDIR /root

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    wget \
    unzip \
    protobuf-compiler \
    libprotobuf-dev \
    build-essential \
    cmake \
    pkg-config \
    gdb \
    vim \
    python3 \
    git \
    gnupg \
 && apt-get -y -q upgrade \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# SGX SDK is installed in /opt/install directory
WORKDIR /opt/intel

ARG SGX_SDK_INSTALLER=sgx_linux_x64_sdk_2.15.100.3.bin

RUN echo "deb [arch=amd64] https://download.01.org/intel-sgx/sgx_repo/ubuntu focal main" >> /etc/apt/sources.list.d/intel-sgx.list \
 && wget -qO - https://download.01.org/intel-sgx/sgx_repo/ubuntu/intel-sgx-deb.key | apt-key add - \
 && apt-get update \
 && apt-get install -y \
    libsgx-launch libsgx-urts 

# Install SGX SDK
RUN wget https://download.01.org/intel-sgx/sgx-linux/2.15/distro/ubuntu18.04-server/$SGX_SDK_INSTALLER \
 && chmod +x  $SGX_SDK_INSTALLER \
 && echo "yes" | ./$SGX_SDK_INSTALLER \
 && rm $SGX_SDK_INSTALLER


# ---------------------------- All of The Source Code download into /sgx -----------------------------------------
# Intel Intel SGX SSL: need SDK build mitigation tools
WORKDIR /sgx
RUN apt-get install -y \
    build-essential ocaml ocamlbuild automake autoconf libtool wget python-is-python3 libssl-dev git cmake perl 
RUN  git clone https://github.com/intel/linux-sgx.git \
 && cd linux-sgx && make preparation && cd .. \ 
 && cp linux-sgx/external/toolset/ubuntu20.04/* /usr/local/bin \
 && git clone https://github.com/intel/intel-sgx-ssl.git \
 && cd intel-sgx-ssl \
 && cd openssl_source && wget https://www.openssl.org/source/openssl-1.1.1q.tar.gz && cd - \
 && cd Linux && make all && make install 

# Build and Install CTK
WORKDIR /sgx
RUN apt update && apt install -y \ 
    make dkms autoconf libcppunit-dev autotools-dev libc6-dev libtool build-essential \
 && git clone https://github.com/intel/crypto-api-toolkit.git \
 && cd crypto-api-toolkit \
 && ./autogen.sh && ./configure && make && make install  

# Build and Install PKCS11 Tool for csr
WORKDIR /sgx
RUN apt-get -y install autoconf-archive autoconf automake libtool pkg-config
RUN git clone https://github.com/Mastercard/pkcs11-tools.git \
 && cd pkcs11-tools && ./bootstrap.sh && ./configure && make &&  make install


 

# Build and Install P11-kit
WORKDIR /sgx
RUN git clone https://github.com/p11-glue/p11-kit.git -b 0.24.1
COPY p11-kit/* /sgx/p11-kit/

RUN apt-get update \
 && apt-get install -y  make gawk autoconf autopoint pkg-config  libffi-dev libtool libtasn1-6-dev gettext libtasn1-bin\
 && git config --global user.email "you@example.com" && git config --global user.name "Your Name" \
 && cd p11-kit && git am 0001-change-to-fit-sgx-ctk.patch &&  ./autogen.sh && ./configure  &&  make install \
 && cp ./.libs/libp11-kit.so.0 /lib/x86_64-linux-gnu/libp11-kit.so.0 \
 && cp ./.libs/libp11-kit.so.0.3.0 /lib/x86_64-linux-gnu/libp11-kit.so.0.3.0 \
 && cd - &&  echo "/usr/local/lib" >> /etc/ld.so.conf && ldconfig

FROM golang:1.17.12 AS go-builder
COPY ./ /sgx/sdewanhsm
WORKDIR /sgx/sdewanhsm
RUN  go build -o hsmserver main.go



#-----------------------------------------------------------------------------------------------------------
FROM ubuntu:focal 
WORKDIR /root

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y wget gnupg opensc \
 && echo "deb [arch=amd64] https://download.01.org/intel-sgx/sgx_repo/ubuntu focal main" >> /etc/apt/sources.list.d/intel-sgx.list \
 && wget -qO - https://download.01.org/intel-sgx/sgx_repo/ubuntu/intel-sgx-deb.key | apt-key add - \
 && apt-get update \
 && env DEBIAN_FRONTEND=noninteractive apt-get install -y \
    libsgx-dcap-ql-dev \
    libsgx-dcap-default-qpl-dev \
    libsgx-quote-ex-dev \
 && apt-get remove -y wget gnupg\
 && apt-get clean -y  && apt autoremove -y\
 && rm -rf /var/lib/apt/lists/* \
 && mkdir -p /opt/intel/cryptoapitoolkit/tokens \
 && chmod -R 1777 /opt/intel/cryptoapitoolkit/tokens \
 && mkdir -p /usr/local/ \
 && echo "/usr/local/lib" >> /etc/ld.so.conf && ldconfig

COPY --from=builder /usr/local/lib/libp11*  /usr/local/lib/
COPY --from=builder /usr/local/bin/p11req   /usr/local/bin/ 
COPY --from=builder /usr/local/bin/p11-kit  /usr/local/bin/ 
COPY --from=builder /usr/local/libexec/p11-kit /usr/local/libexec/p11-kit  
COPY --from=builder /usr/local/libexec/p11-kit/ /usr/local/libexec/p11-kit/


COPY --from=builder /usr/local/lib/pkcs11/ /usr/local/lib/pkcs11/
COPY --from=builder /usr/lib/x86_64-linux-gnu/libp11* /usr/lib/x86_64-linux-gnu/


WORKDIR /sgx
COPY --from=go-builder /sgx/sdewanhsm/hsmserver /sgx/hsmserver

ENTRYPOINT  bash -c "/sgx/hsmserver"


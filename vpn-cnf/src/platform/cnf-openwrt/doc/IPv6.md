# SDEWAN IPv6 Support

## Overview
We only support a simple mode IPv6 data path connection as the figure shown below:
<img src="../../platform/images/cnf_ipv6_simple_mode.png"/>

To do that, please:
1. Configure the dual stack environment properly for all dependencies (Kubernetes, Nodus, CNI, etc.). Details as below.
2. Follow our Helm chart [example](../../platform/deployment/helm/sdewan_cnf_ipv6) to set up your own one.

## 1. Background

Advantages compared against IPv4:
1.	Sufficient addresses (inexhaustible for current applications). 
2.	Enhanced auto-addressing (simplified provisioning). Simplified address auto configuration mechanism (e.g. SLAAC) is suitable for IoT devices that doesn’t support DHCP.
3.	Improved management of layer 2 to layer 3 mappings.

Industry trends:

  [IPv6 Adoptions – Statistics from Google](https://www.google.com/intl/en/ipv6/statistics.html)

  [IPv6 Adoption in 2021 | RIPE Labs](https://labs.ripe.net/author/stephen_strowes/ipv6-adoption-in-2021/)

  [IPv6 deployment - Wikipedia](https://en.wikipedia.org/wiki/IPv6_deployment)


## 2. Dependencies

IPv6 readiness assurance of all dependencies.

### 2.1 Linux Kernel

IPv6 ready and mature for years.

sysctl set ipv6 forwarding and enable for interfaces.

### 2.2 Linux Tool Chains

IPv6 ready and mature for years.

iproute2 (e.g. ip route)

iptables

### 2.3 Kubernetes

IPv6 ready with dual stack support.

Using recent version of Kubernetes >= 1.23.6

Lower version has some [bug]([kube-proxy: fix duplicate port opening by tnqn · Pull Request #107413 · kubernetes/kubernetes · GitHub](https://github.com/kubernetes/kubernetes/pull/107413)).

Turn on dual stack when kubeadm init.

### 2.4 Calico

IPv6 ready with dual stack support [Configure dual stack or IPv6 only (tigera.io)](https://projectcalico.docs.tigera.io/networking/ipv6).

Calico supports IPv6 out of box [Frequently asked questions (tigera.io)](https://projectcalico.docs.tigera.io/reference/faq#does-calico-work-with-ipv6)

Correct configuration in yaml file.

Something like this:
```yaml
ubuntu@p4-overlay:/mnt/ipv6$ cat calico-custom-resources-v3.23.1.yaml
# This section includes base Calico installation configuration.
# For more information, see: https://projectcalico.docs.tigera.io/v3.23/reference/installation/api#operator.tigera.io/v1.Installation
apiVersion: operator.tigera.io/v1
kind: Installation
metadata:
  name: default
spec:
  # Configures Calico networking.
  calicoNetwork:
    # Note: The ipPools section cannot be modified post-install.
    ipPools:
    - blockSize: 26
      #cidr: 192.168.0.0/16
      cidr: 10.151.142.0/18
      encapsulation: VXLANCrossSubnet
      natOutgoing: Enabled
      nodeSelector: all()
    - blockSize: 122
      cidr: fd00:1234:a890:5678::/120
      #encapsulation: None
      encapsulation: VXLANCrossSubnet
      natOutgoing: Enabled
      nodeSelector: all()

---

# This section configures the Calico API server.
# For more information, see: https://projectcalico.docs.tigera.io/v3.23/reference/installation/api#operator.tigera.io/v1.APIServer
apiVersion: operator.tigera.io/v1
kind: APIServer
metadata:
  name: default
spec: {}

ubuntu@p4-overlay:/mnt/ipv6$

```


### 2.5 Nodus

IPv6 ready with dual stack support.

Relative [new feature](https://github.com/akraino-edge-stack/icn-nodus/blob/master/doc/ipv6.md).


### 2.6 StrongSwan

IPv6 ready and mature for years.

Verified works in our environment.

VTI mechanism verified.

Correct configuration as our requirment.

### 2.7 OpenWRT

IPv6 ready and mature for years.

Lives in Linux eco-system (Linux kernel and tool chains).

### 2.8 MongoDB

IPv6 ready [IP Binding — MongoDB Manual](https://www.mongodb.com/docs/manual/core/security-mongodb-configuration/)
Additional option for IPv6 is needed (app server implemented in socket program distinguish IPv4 and IPv6, listen on IPv4 is one path, listen on IPv6 is another).

### 2.9 etcd

IPv6 ready. Should works as K8S support dual stack.




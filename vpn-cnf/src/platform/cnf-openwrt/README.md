```
SPDX-License-Identifier: Apache-2.0
Copyright (c) 2021 Intel Corporation
```

# cnf-openwrt

sdewan cnf docker image for SDEWAN solution

# folder structure

* src: includes all file to generate sdewan docker image
* examples: sample yaml file to create CNF
* doc: documents
* README.md: this file

# Build

Requirements:
* docker

Steps:


* Build docker image
```sh 
  cd src
  make cnf
```

Note: After build, the docker image will be imported as integratedcloudnative/sdewan-cnf:master

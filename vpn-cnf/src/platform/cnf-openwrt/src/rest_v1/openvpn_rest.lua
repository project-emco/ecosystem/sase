--- SPDX-License-Identifier: Apache-2.0
--- Copyright (c) 2021 Intel Corporation

module("luci.controller.rest_v1.openvpn_rest", package.seeall)

local uci = require "luci.model.uci"
json = require "luci.jsonc"
io = require "io"
sys = require "luci.sys"
utils = require "luci.controller.rest_v1.utils"

uci_conf = "openvpn"

openvpn_validator ={
    config_type="openvpn",
    {name="name",required=true},
    {name="enabled"},
    {name="client"},
    {name="dev",required=true,validator=function(value) return utils.in_array(value, {"tap", "tun"}) end,message="Invalid dev"},
    {name="proto",required=true,validator=function(value) return utils.in_array(value, {"tcp", "udp"}) end,message="Invalid proto"},
    {name="remote",required=true},
    {name="ca",required=true},
    {name="cert",required=true},
    {name="key",required=true},
    {name="tls_crypt"},
}


 openvpn_processor = {
    openvpn={create="create_openvpn", delete="delete_openvpn", validator=openvpn_validator},
    configuration=uci_conf,
}


function index()
    ver = "v1"
    configuration = "openvpn"
    entry({"sdewan", configuration, ver, "openvpns"}, call("handle_request")).leaf = true
end



function handle_request()
    local conf = io.open("/etc/config/" .. uci_conf, "r")
    if conf == nil then
        conf = io.open("/etc/config/" .. uci_conf, "w")
    end
    conf:close()
    local handler = utils.handles_table[utils.get_req_method()]
    if handler == nil then
        utils.response_error(405, "Method Not Allowed")
    else
        return utils[handler](_M,openvpn_processor)
    end
end



function create_openvpn(vpnConf)
    if save_cert(vpnConf) then
        vpnConf.ca=file_name(vpnConf.name)["ca"]
        vpnConf.cert=file_name(vpnConf.name)["cert"]
        vpnConf.key=file_name(vpnConf.name)["key"]
        vpnConf.tls_crypt=file_name(vpnConf.name)["tls"]
        vpnConf.enabled="1"
        vpnConf.client="1"
        local res, code, msg = utils.create_uci_section(uci_conf, openvpn_validator, "openvpn", vpnConf)
        if res == false then
            delete_cert(vpnConf.name)
            uci:revert(uci_conf)
            return res, code, msg
        end
        uci:save(uci_conf)
        uci:commit(uci_conf)
        return true
    else
        return false
    end
end

function delete_openvpn(name)
    ---check whether openvpnConf is defined
    local openvpnConf = utils.get_object(_M, openvpn_processor, "openvpn", name)
    if openvpnConf == nil then
        return false, 404, "openvpn" .. name .. " is not defined"
    end
    delete_cert(name)
    utils.delete_uci_section(uci_conf, openvpn_validator, openvpnConf, "openvpn")
    ---commit change
    uci:save(uci_conf)
    uci:commit(uci_conf)
    return true

end

function save_cert(vpnConf)
    utils.decode_and_save_to_file(vpnConf.ca,file_name(vpnConf.name)["ca"])
    utils.decode_and_save_to_file(vpnConf.cert,file_name(vpnConf.name)["cert"])
    utils.decode_and_save_to_file(vpnConf.key,file_name(vpnConf.name)["key"])
    if vpnConf.tls_crypt~=nil then
        utils.decode_and_save_to_file(vpnConf.tls_crypt,file_name(vpnConf.name)["tls"])
    end
    return true
end

function delete_cert(name)
    os.remove(file_name(name)["ca"])
    os.remove(file_name(name)["cert"])
    os.remove(file_name(name)["key"])
    os.remove(file_name(name)["tls"])
    return true

end
function file_name(name)
    local certfile={}
    certfile["ca"]="/etc/openvpn/" .. name.."-ca.crt"
    certfile["cert"]="/etc/openvpn/" .. name.."-client.crt"
    certfile["key"]="/etc/openvpn/" .. name.."-client.key"
    certfile["tls"]="/etc/openvpn/" .. name.."-client.pem"
    return certfile
end


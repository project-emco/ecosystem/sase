--- SPDX-License-Identifier: Apache-2.0
--- Copyright (c) 2022 Intel Corporation

module("luci.controller.rest_v1.greserver_rest", package.seeall)

local uci = require "luci.model.uci"
json = require "luci.jsonc"
io = require "io"
sys = require "luci.sys"
utils = require "luci.controller.rest_v1.utils"

uci_conf = "greserver"

greserver_validator ={
    config_type="greserver",
    {name="name",required=true},
    {name="localIP",required=true,alidator=function(value) return utils.is_valid_ip_address(value) end,message="Invalid localIP"},
    {name="remoteIP",required=true,alidator=function(value) return utils.is_valid_ip_address(value) end,message="Invalid remoteIP"},
    {name="tableName"},
    {name="svcSubnet",alidator=function(value) return utils.is_valid_ip_address(value) end,message="Invalid svcSubnet"},
}    

greserver_processor = {
    greserver={create="create_greserver", delete="delete_greserver", validator=greserver_validator},
    configuration=uci_conf,
}

function index()
    ver = "v1"
    configuration = "greserver"
    entry({"sdewan", configuration, ver, "greservers"}, call("handle_request")).leaf = true
end

function handle_request()
   local conf = io.open("/etc/config/" .. uci_conf, "r")
    if conf == nil then
        conf = io.open("/etc/config/" .. uci_conf, "w")
    end
    conf:close()
    local handler = utils.handles_table[utils.get_req_method()]
    if handler == nil then
        utils.response_error(405, "Method Not Allowed")
    else
        return utils[handler](_M,greserver_processor)
    end
end

function greserver_command(rule, op)
    local gre_list={}
    local iprule_list={}
    local name = rule["name"]
    local localIP = rule["localIP"]
    local remoteIP = rule["remoteIP"]
    local tableName= rule["tableName"]
    local svcSubnet= rule["svcSubnet"]

    if op == "create" then
        gre_list[1]= "ip tunnel add ".. name .." mode gre local " .. localIP.. " remote " .. remoteIP .. " ttl 255"
        gre_list[2]=" ip addr add " .. localIP.. " dev gre2"
        gre_list[3]= "ip link set " .. name .. " up "
        if svcSubnet ~="" and svcSubnet ~=nil then
            iprule_list[1]=" ip route add " .. svcSubnet .. " dev " .. name .. " scope link "
            iprule_list[2]=" iptables -t nat -A POSTROUTING -o " .. name .. " -j SNAT --to-source ".. localIP 
        else
            iprule_list[1]=" ip rule add iif " .. name .. " lookup "  .. tableName
            iprule_list[2]=" ip route add " .. remoteIP .." dev ".. name .. "  table " ..tableName
        end
    else
        gre_list[1]="ip tunnel del " .. name
        if svcSubnet ~="" and svcSubnet ~=nil then
            iprule_list[1]="ip route delete " .. svcSubnet .. " dev " .. name .. " scope link"
            iprule_list[2]="iptables -t nat -D POSTROUTING -o " .. name .. " -j SNAT --to-source ".. localIP 
        else
            iprule_list[1]="ip rule delete iif " .. name .. " lookup "  .. tableName
            iprule_list[2]="ip route delete " .. remoteIP .." dev ".. name .. "  table " ..tableName
        end
    end
    return gre_list, iprule_list
end

function create_greserver(gretunnel)
    local res, code, msg = utils.create_uci_section(uci_conf, greserver_validator, "greserver", gretunnel)
    if res == false then
        uci:revert(uci_conf)
        return res, code, msg
    end
    local gre_list, iprule_list = greserver_command(gretunnel, "create")
    for _,comm in ipairs(gre_list) do
        os.execute(comm)
        utils.log(comm)
    end
    for _,comm in ipairs(iprule_list) do
        os.execute(comm)
        utils.log(comm)
    end
    uci:save(uci_conf)
    uci:commit(uci_conf)
    return true
end

function delete_greserver(name)
    -- check whether rule is defined
    local gretunnel = utils.get_object(_M, greserver_processor, "greserver", name)
    if gretunnel == nil then
        return false, 404, " gretunnel " .. name .. " is not defined "
    end

    -- delete  rule
    local gre_list, iprule_list = greserver_command(gretunnel, "delete")
    for _,comm in ipairs(gre_list) do
        os.execute(comm)
        utils.log(comm)
    end
    for _,comm in ipairs(iprule_list) do
        os.execute(comm)
        utils.log(comm)
    end

    utils.delete_uci_section(uci_conf, greserver_validator, gretunnel, "greserver")
    -- commit change
    uci:save(uci_conf)
    uci:commit(uci_conf)

    return true
end


--- SPDX-License-Identifier: Apache-2.0
--- Copyright (c) 2021 Intel Corporation

module("luci.controller.rest_v1.pkcs11", package.seeall)

local json = require "luci.jsonc"
local utils = require "luci.controller.rest_v1.utils"
local sdewan_hsm_pkcs11 = "http://127.0.0.1:8081/pkcs11"

function index()
    ver = "v1"
    configuration = "pkcs11"
    entry({ "sdewan", configuration, ver, "csr" }, call("handle_csr_request")).leaf = true
    entry({ "sdewan", configuration, ver, "cert" }, call("handle_cert_request")).leaf = true
end

function handle_csr_request()
    local method = utils.get_req_method()
    if method == "POST" then
        return create_csr()
    else
        utils.response_error(405, "Method Not Allowed")
    end
end

function handle_cert_request()
    local method = utils.get_req_method()
    if method == "POST" then
        return add_cert()
    else
        utils.response_error(405, "Method Not Allowed")
    end
end

function create_csr()
    local req = utils.get_request_body_object()
    local req_body = json.stringify(req)

    utils.log("[pkcs11] genereate csr")
    utils.log(req_body)
    code, resp = utils.http_request {
        base_url = sdewan_hsm_pkcs11,
        endpoint = "/csr",
        method = "POST",
        source = ltn12.source.string(req_body),
        headers = {
            ["Content-Type"] = "application/json",
            ["Content-Length"] = #req_body
        }
    }

    luci.http.status(code)
    luci.http.prepare_content("application/text")
    luci.http.write(resp[1])
end

function add_cert()
    local req = utils.get_request_body_object()
    local code, resp = save_cert_to_hsm(req) 

    luci.http.status(code)
    luci.http.prepare_content("application/text")
    luci.http.write(resp[1])
end

function  save_cert_to_hsm(req)
    local req_body = json.stringify(req)
    utils.log("[pkcs11] add cert")
    utils.log(req_body)
    local code, resp = utils.http_request {
        base_url = sdewan_hsm_pkcs11,
        endpoint = "/cert",
        method = "POST",
        source = ltn12.source.string(req_body),
        headers = {
            ["Content-Type"] = "application/json",
            ["Content-Length"] = #req_body
        }
    }
    return code,resp
end

function  delete_cert_on_hsm(req)
    local req_body = json.stringify(req)
    utils.log("[pkcs11] delete cert")
    utils.log(req_body)
    local code, resp = utils.http_request {
        base_url = sdewan_hsm_pkcs11,
        endpoint = "/cert",
        method = "DELETE",
        source = ltn12.source.string(req_body),
        headers = {
            ["Content-Type"] = "application/json",
            ["Content-Length"] = #req_body
        }
    }
    return code,resp
end


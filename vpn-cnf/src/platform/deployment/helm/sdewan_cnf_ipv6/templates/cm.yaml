#/* Copyright (c) 2021 Intel Corporation, Inc
# *
# * Licensed under the Apache License, Version 2.0 (the "License");
# * you may not use this file except in compliance with the License.
# * You may obtain a copy of the License at
# *
# *     http://www.apache.org/licenses/LICENSE-2.0
# *
# * Unless required by applicable law or agreed to in writing, software
# * distributed under the License is distributed on an "AS IS" BASIS,
# * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# * See the License for the specific language governing permissions and
# * limitations under the License.
# */

apiVersion: v1
data:
  entrypoint.sh: |-
    #!/bin/bash
    # Always exit on errors.
    set -ex

    source /etc/ipv6utils

    function iface_add_addr() {
      local iface_name=$1
      local ip_addr=$2

      if [ "$ip_addr" = "" ] ; then
        return 0
      fi
      if [ "$iface_name" = "" ] ; then
        return 0
      fi

      local ip_version=$(get_ip_version $ip_addr)
      echo IP version is: $ip_version, Address is: $ip_addr, Interface name is: $iface_name

      if [ $ip_version == "6" ] || [ $ip_version == "4" ] ; then
        ip -$ip_version addr add $ip_addr dev $iface_name
        #ip a
      fi
    }

{{- if .Values.providerCIDR}}
    provider_cidr_ipv4=$(get_ip_from_pair {{ .Values.providerCIDR }} "4")
    provider_cidr_ipv6=$(get_ip_from_pair {{ .Values.providerCIDR }} "6")
{{- end }}
{{- if .Values.publicIpAddress }}
    public_ipv4=$(get_ip_from_pair {{ .Values.publicIpAddress }} "4")
    public_ipv6=$(get_ip_from_pair {{ .Values.publicIpAddress }} "6")
{{- end }}
{{- if .Values.defaultCIDR }}
    default_cidr_ipv4=$(get_ip_from_pair {{ .Values.defaultCIDR }} "4")
    default_cidr_ipv6=$(get_ip_from_pair {{ .Values.defaultCIDR }} "6")
{{- end }}
{{- if .Values.kubeApiServerIP }}
    apiserver_ipv6=$(get_ip_from_pair {{ .Values.kubeApiServerIP }} "6")
{{- end }}

    provider_support_ipv4=false
    if [ ! -z "${provider_cidr_ipv4}" ] ; then
      provider_support_ipv4=true
    fi
    provider_support_ipv6=false
    if [ ! -z "${provider_cidr_ipv6}" ] ; then
      provider_support_ipv6=true
    fi


    sysctl -w net.ipv4.ip_forward=1
    if [ $provider_support_ipv6 == "true" ] ; then
      sysctl -w net.ipv6.conf.all.disable_ipv6=0
      sysctl -w net.ipv6.conf.default.disable_ipv6=0
      sysctl -w net.ipv6.conf.all.forwarding=1
    fi

    echo "" > /etc/config/network
    cat > /etc/config/mwan3 <<EOF
    config globals 'globals'
        option mmx_mask '0x3F00'
        option local_source 'lan'
    EOF

    provider_ipv4=$(echo $provider_cidr_ipv4 | cut -d/ -f1)
    provider_ipv6=$(echo $provider_cidr_ipv6 | cut -d/ -f1)
    sep="."
    suf="0"

    function init_etc_conf_net6() {
      # It simply works with empty /etc/config/network for IPv6.
      # So we intentionally set that file as empty for now.
      # TODO: Check if it's necessary to set the /etc/config/network properly
      echo > /etc/config/network
    }

    eval "networks=$(grep nfn-network /tmp/podinfo/annotations | awk  -F '=' '{print $2}')"
    if [ $provider_support_ipv6 == "true" ] ; then
      init_etc_conf_net6
    else
      for net in $(echo -e $networks | jq -c ".interface[]")
      do
        interface=$(echo $net | jq -r .interface)
        ipaddr=$(ifconfig $interface | awk '/inet/{print $2}' | cut -f2 -d ":" | awk 'NR==1 {print $1}')
        vif="$interface"
        netmask=$(ifconfig $interface | awk '/inet/{print $4}'| cut -f2 -d ":" | head -1)
        cat >> /etc/config/network <<EOF
      config interface '$vif'
          option ifname '$interface'
          option proto 'static'
          option ipaddr '$ipaddr'
          option netmask '$netmask'
    EOF
      done
    fi

    if [ -f "/tmp/sdewan/account/password" ]; then
        echo "Changing password ..."
        pass=$(cat /tmp/sdewan/account/password)
        echo root:$pass | chpasswd -m
    fi

    if [ -d "/tmp/sdewan/serving-certs/" ]; then
        echo "Configuration certificates ..."
        cp /tmp/sdewan/serving-certs/tls.crt /etc/uhttpd.crt
        cp /tmp/sdewan/serving-certs/tls.key /etc/uhttpd.key
    fi

    /sbin/procd &
    /sbin/ubusd &
    iptables -t nat -L
    sleep 1
    /etc/init.d/rpcd start
    /etc/init.d/dnsmasq start
    /etc/init.d/network start
    /etc/init.d/odhcpd start
    /etc/init.d/uhttpd start
    /etc/init.d/log start
    /etc/init.d/dropbear start
    /etc/init.d/mwan3 restart
    /etc/init.d/firewall restart

    # Workaround: Nodus doesn't support multiple IP addresses in "ipAddress" (e.g. "fd00:1234::29,10.10.70.29")
    # Instead, we need to set it ourselves with a secondary parameter "ipAddress2".
    function set_ipv4_addr() {
        local ipv4_addr=""
        local ipv4_addr2=""
      {{- range $v := .Values.nfn }}
        ipv4_addr=$(get_ip_from_pair {{ $v.ipAddress }} "4")
        if [ ! -z "${ipv4_addr}" ] ; then
          iface_add_addr {{ $v.interface }} $ipv4_addr
        fi
        ipv4_addr2=$(get_ip_from_pair {{ $v.ipAddress2 }} "4")
        if [ ! -z "${ipv4_addr2}" ] ; then
          iface_add_addr {{ $v.interface }} $ipv4_addr2
        fi
      {{- end }}

      if [ ! -z "${provider_cidr_ipv4}" ] ; then
        ip route add $provider_cidr_ipv4 dev {{ .Values.providerInterface }}
      fi
    }

    set_ipv4_addr

    function config_ipv4() {
      local provider_cidr=$1
      local public_ip_address=$2
      local default_cidr=$3

      if [ ! -z "${provider_cidr}" ] && [ ! -z "${provider_ipv4}" ] ; then
        #defaultip=$(grep "\podIP\b" /tmp/podinfo/annotations | cut -d/ -f2 | cut -d'"' -f2)
        for net in $(echo -e $networks | jq -c ".interface[]")
        do
          interface=$(echo $net | jq -r .interface)
          ipaddr=$(ifconfig $interface | awk '/inet/{print $2}' | cut -f2 -d ":" | awk 'NR==1 {print $1}')
          echo $ipaddr | ( IFS="." read -r var1 var2 var3 var4; CIDR="$var1$sep$var2$sep$var3$sep$suf"; \
            if [ "${CIDR}" = "${provider_ipv4}" ] ; then iptables -t nat -A POSTROUTING -o $interface -d $provider_cidr -j SNAT --to-source $ipaddr; fi)
        done
      fi

      if [ ! -z "${public_ip_address}" ] ; then
        iptables -t nat -I PREROUTING 1 -m tcp -p tcp -d $public_ip_address --dport 6443 -j DNAT --to-dest 10.96.0.1:443
      fi

      if [ ! -z "${default_cidr}" ] ; then
        ip rule add from $default_cidr lookup 40
        #ip rule add from $defaultip lookup main
      fi

    }

    # Here CNF serves as an endpoint that route the request to kube-apiserver.
    # We only support IPv4 over IPv6 now which means that all other IP in clusters are IPv4 whereas CNFs use IPv6 address for underlay network.
    # In this situation, we need to route the request to kube-apiserver's IPv6 endpoint. It should work since the Kubernetes runs as dual stack.
    # But Kubernetes doesn't support well for DualStack apiserver https://github.com/kubernetes/enhancements/issues/2438.
    # So workaround for our case is using apiserver pod directly instead of apiserver Service (Service name is "kubernetes").
    function forward_to_apiserver() {
      local cnf_public_ip=$1
      local apiserver_ip=$2
      ip6tables -t nat -I PREROUTING 1 -m tcp -p tcp -d $cnf_public_ip --dport 6443 -j DNAT --to-destination $apiserver_ip
      ip6tables -t nat -I POSTROUTING -j MASQUERADE
    }

    # The original intention here is that:
    #   If the packet will be sent out with destination address of the ProviderNetwork CIDR via the interface created by Nodus,
    #   Then we will use SNAT to set the source address of that packet as the IP of this interface that's attached to that ProviderNetwork.
    function snat_interface() {
      if [ -z "${provider_ipv6}" ] ; then
        return 0
      fi
      local interface=$1
      local dst_cidr=$2
      addr_info=$(ip -j a show dev $interface | jq -r '.[0].addr_info[]  | select((.family == "inet6") and (.scope != "link"))')
      addr6=$(echo $addr_info | jq -r '.local')
      prefix6=$(echo $addr_info | jq -r '.prefixlen')
      prefix_iface=$(ipv6_get_addr_prefix $addr6 $prefix6)
      prefix_provider=$(ipv6_get_addr_prefix $provider_ipv6 $prefix6)
      if [ "${prefix_iface}" = "${prefix_provider}" ] ; then
        ip6tables -t nat -A POSTROUTING -o $interface -d $dst_cidr -j SNAT --to-source $addr6
      fi
    }

    function ip_rule_add() {
      local from_ip=$1
      local table_id=$2
      local ip_version=$(get_ip_version $from_ip)
      if [ $ip_version == "6" ] || [ $ip_version == "4" ] ; then
        ip -$ip_version rule add from $from_ip lookup $table_id
      fi
    }

    function config_ipv6() {
      local provider_cidr=$1
      local public_ip=$2
      local default_cidr=$3

      if [ ! -z "${provider_cidr}" ] ; then
        for net in $(echo -e $networks | jq -c ".interface[]")
        do
          interface=$(echo $net | jq -r .interface)
          #snat_interface $interface {{ .Values.providerCIDR }}
          snat_interface $interface $provider_cidr
        done
      fi

      if [ ! -z "${public_ip}" ] ; then
        forward_to_apiserver $public_ip $apiserver_ipv6
      fi

      if [ ! -z "${default_cidr}" ] ; then
        ip_rule_add $default_cidr 40
      fi
    }

    function add_cnf_ip_rule() {
      cni_pod_ips=$(grep "cni.projectcalico.org/podIPs" /tmp/podinfo/annotations | sed 's;cni.projectcalico.org/podIPs="\(.\+\)";\1;g')
      cnf_ipv4=$(get_ip_from_pair $cni_pod_ips "4")
      cnf_ipv6=$(get_ip_from_pair $cni_pod_ips "6")
      ip_rule_add $cnf_ipv4 main
      ip_rule_add $cnf_ipv6 main
    }

    if [ $provider_support_ipv4 == "true" ] ; then
      config_ipv4 $provider_cidr_ipv4 $public_ipv4 $default_cidr_ipv4
    fi
    if [ $provider_support_ipv6 == "true" ] ; then
      config_ipv6 $provider_cidr_ipv6 $public_ipv6 $default_cidr_ipv6
    fi
    add_cnf_ip_rule

    echo "Entering sleep... (success)"
    # Sleep forever.
    while true; do sleep 100; done
kind: ConfigMap
metadata:
  name: sdewan-safe-sh
  namespace: default


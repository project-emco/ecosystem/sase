/*
 * Copyright 2020 Intel Corporation, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package resource

import (
	//"encoding/base64"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1 "k8s.io/api/core/v1"
	"strconv"
)

type IssuerConditionType string

const (
	// IssuerConditionReady represents the fact that a given Issuer condition
	// is in ready state and able to issue certificates.
	// If the `status` of this condition is `False`, CertificateRequest controllers
	// should prevent attempts to sign certificates.
	IssuerConditionReady IssuerConditionType = "Ready"
)

type IssuerResource struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TCSIssuerSpec   `json:"spec,omitempty"`
	Status TCSIssuerStatus `json:"status,omitempty"`
}

type TCSIssuerSpec struct {
	// +optional
	Labels map[string]string `json:"labels,omitempty"`

	SecretName string `json:"secretName,omitempty"`
	SelfSignCertificate bool `json:"selfSign,omitempty"`
}

type TCSIssuerStatus struct {
	// +optional
	Conditions []TCSIssuerCondition `json:"conditions,omitempty"`
}

type TCSIssuerCondition struct {
	Type IssuerConditionType `json:"type"`

	Status v1.ConditionStatus `json:"status"`

	// +optional
	LastTransitionTime *metav1.Time `json:"lastTransitionTime,omitempty"`

	// +optional
	Reason string `json:"reason,omitempty"`

	// +optional
	Message string `json:"message,omitempty"`
}

func (c *IssuerResource) GetName() string {
	return c.ObjectMeta.Name
}

func (c *IssuerResource) GetType() string {
	return "QuoteAttestation"
}

func (c *IssuerResource) ToYaml(target string) string {
	base := `apiVersion: tcs.intel.com/v1alpha1
kind: TCSIssuer
metadata:
  name: ` + c.ObjectMeta.Name + `
  namespace: ` + c.ObjectMeta.Namespace + `
  labels:
    sdewanPurpose: ` + SdewanPurpose + `
    targetCluster: ` + target + `
spec:
  secretName: ` + c.Spec.SecretName + `
  selfSign: ` + strconv.FormatBool(c.Spec.SelfSignCertificate)

	return string(base)
}

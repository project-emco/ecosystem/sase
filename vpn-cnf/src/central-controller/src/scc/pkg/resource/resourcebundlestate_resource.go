/*
 * Copyright 2020 Intel Corporation, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package resource

import (
	//"encoding/base64"
	//metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	//v1 "k8s.io/api/core/v1"
	//"strconv"
	mv1alpha1 "gitlab.com/project-emco/core/emco-base/src/monitor/pkg/apis/k8splugin/v1alpha1"
)


type ResourceBundleStateResource struct {
	mv1alpha1.ResourceBundleState `json:",inline"`
}

func (c *ResourceBundleStateResource) GetName() string {
	return c.ObjectMeta.Name
}

func (c *ResourceBundleStateResource) GetType() string {
	return "ResourceBundleState"
}

func (c *ResourceBundleStateResource) ToYaml(target string) string {
	base := `apiVersion: k8splugin.io/v1alpha1
kind: ResourceBundleState
metadata:
  name: ` + c.ObjectMeta.Name + `
  namespace: ` + c.ObjectMeta.Namespace + `
  labels:
    sdewanPurpose: ` + SdewanPurpose + `
    targetCluster: ` + target + `
spec:
  selector: 
    matchLabels: `
        for k, v := range c.Spec.Selector.MatchLabels {
		base += `
      ` + k + `: ` + v
	}

	return string(base)
}

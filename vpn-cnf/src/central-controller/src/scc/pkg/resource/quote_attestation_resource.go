/*
 * Copyright 2020 Intel Corporation, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package resource

import (
	"encoding/base64"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

type ConditionType string

const (
	// Approved indicates the request was approved and should be issued by the signer.
	AttestationSuccess ConditionType = "Success"
	// Failed indicates the signer failed to issue the certificate.
	AttestationFailed ConditionType = "Failed"
)

type QuoteAttestationResource struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   QuoteAttestationSpec   `json:"spec,omitempty"`
	Status QuoteAttestationStatus `json:"status,omitempty"`
}

type QuoteAttestationSpec struct {
	Quote        []byte   `json:"quote"`
	QuoteVersion string   `json:"quoteVersion,omitempty"`
	ServiceID    string   `json:"serviceId"`
	PublicKey    []byte   `json:"publicKey"`
	SignerNames  []string `json:"signerNames"`
}

type QuoteAttestationStatus struct {
	Condition QuoteAttestationCondition         `json:"condition,omitempty"`
	Secrets   map[string]QuoteAttestationSecret `json:"secrets,omitempty"`
}

type QuoteAttestationSecret struct {
	SecretName string `json:"secretName,omitempty"`
	SecretType string `json:"secretType,omitempty"`
}

type QuoteAttestationCondition struct {
	Type           ConditionType `json:"type,omitempty"`
	State          string        `json:"state,omitempty"`
	Message        string        `json:"message,omitempty"`
	LastUpdateTime metav1.Time   `json:"lastUpdateTime,omitempty"`
}

func (c *QuoteAttestationResource) GetName() string {
	return c.ObjectMeta.Name
}

func (c *QuoteAttestationResource) GetType() string {
	return "QuoteAttestation"
}

func (c *QuoteAttestationResource) ToYaml(target string) string {
	//TODO: change the apiVersion
	base := `apiVersion: experimental.cert-manager.io/v1alpha3
kind: QuoteAttestation
metadata:
  name: ` + c.ObjectMeta.Name + `
  namespace: ` + c.ObjectMeta.Namespace + `
  labels:
    sdewanPurpose: ` + SdewanPurpose + `
    targetCluster: ` + target + `
spec:    
  publicKey: ` + base64.StdEncoding.EncodeToString(c.Spec.PublicKey) + `
  quote: ` + base64.StdEncoding.EncodeToString(c.Spec.Quote) + `
  quoteVersion: ` + c.Spec.QuoteVersion + `
  serviceId: ` + c.Spec.ServiceID + `
  signerNames: `
	if len(c.Spec.SignerNames) > 0 {
		for _, v := range c.Spec.SignerNames {
			base += `
  - ` + string(v)
		}
	}

	if len(c.Status.Secrets) > 0 {
		base += `
status:
  condition:
    type: ` + string(c.Status.Condition.Type) + `
    state: ` + c.Status.Condition.State + `
    message: ` + c.Status.Condition.Message + `
    lastUpdateTime: ` + c.Status.Condition.LastUpdateTime.Format(time.RFC3339) + `
  secrets: `
		for k, v := range c.Status.Secrets {
			base += `
    ` + k + `:
      secretName: ` + v.SecretName + `
      secretType: ` + v.SecretType
		}
	}

	return base
}

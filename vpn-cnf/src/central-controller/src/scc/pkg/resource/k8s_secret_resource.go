/*
 * Copyright 2020 Intel Corporation, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package resource

import (
	"k8s.io/api/core/v1"
)

type SecretResource struct {
	v1.Secret `json:",inline"`
}

func (c *SecretResource) GetName() string {
	return c.ObjectMeta.Name
}

func (c *SecretResource) GetType() string {
	return "K8sSecret"
}

func (c *SecretResource) ToYaml(target string) string {
	base := `apiVersion: v1
kind: Secret
metadata:
  name: ` + c.ObjectMeta.Name + `
  namespace: ` + c.ObjectMeta.Namespace + `
  labels:
    sdewanPurpose: ` + SdewanPurpose + `
    targetCluster: ` + target + `
type: ` + string(c.Type) + `
data: `
	if len(c.Data) > 0 {
		for k, v := range c.Data {
			base += `
  ` + k + `: ` + string(v)
		}
	}

	return base
}

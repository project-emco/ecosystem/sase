/*
 * Copyright 2020 Intel Corporation, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package manager

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"log"

	"github.com/akraino-edge-stack/icn-sdwan/central-controller/src/scc/pkg/module"
	pkgerrors "github.com/pkg/errors"
	"gitlab.com/project-emco/core/emco-base/src/orchestrator/pkg/infra/db"
	mtypes "gitlab.com/project-emco/core/emco-base/src/orchestrator/pkg/module/types"
)

const DEFAULTPORT = "6443"

type HubObjectKey struct {
	OverlayName string `json:"overlay-name"`
	HubName     string `json:"hub-name"`
}

// HubObjectManager implements the ControllerObjectManager
type HubObjectManager struct {
	BaseObjectManager
}

func NewHubObjectManager() *HubObjectManager {
	return &HubObjectManager{
		BaseObjectManager{
			storeName:      StoreName,
			tagMeta:        "hub",
			depResManagers: []ControllerObjectManager{},
			ownResManagers: []ControllerObjectManager{},
		},
	}
}

func (c *HubObjectManager) GetResourceName() string {
	return HubResource
}

func (c *HubObjectManager) IsOperationSupported(oper string) bool {
	return true
}

func (c *HubObjectManager) CreateEmptyObject() module.ControllerObject {
	return &module.HubObject{}
}

func (c *HubObjectManager) GetStoreKey(m map[string]string, t module.ControllerObject, isCollection bool) (db.Key, error) {
	overlay_name := m[OverlayResource]
	key := HubObjectKey{
		OverlayName: overlay_name,
		HubName:     "",
	}

	if isCollection == true {
		return key, nil
	}

	to := t.(*module.HubObject)
	meta_name := to.Metadata.Name
	res_name := m[HubResource]

	if res_name != "" {
		if meta_name != "" && res_name != meta_name {
			return key, pkgerrors.New("Resource name unmatched metadata name")
		}

		key.HubName = res_name
	} else {
		if meta_name == "" {
			return key, pkgerrors.New("Unable to find resource name")
		}

		key.HubName = meta_name
	}

	return key, nil
}

func (c *HubObjectManager) ParseObject(r io.Reader) (module.ControllerObject, error) {
	var v module.HubObject
	err := json.NewDecoder(r).Decode(&v)

	// initial Status
	v.Status.Data = make(map[string]string)
	return &v, err
}

func (c *HubObjectManager) CreateObject(m map[string]string, t module.ControllerObject) (module.ControllerObject, error) {
	overlay := GetManagerset().Overlay
	certManager := GetManagerset().Cert
	overlay_name := m[OverlayResource]
	to := t.(*module.HubObject)
	hub_name := to.Metadata.Name
	chann := overlay.Chann.C[overlay_name]

	//Todo: Check if public ip can be used.
	var local_public_ip string

	if to.Specification.KubeConfig == "" {
		var gitOpsParams mtypes.GitOpsProps
		gitOpsParams.GitOpsType = to.Specification.GitOpsParam.GitOpsType
		gitOpsParams.GitOpsReferenceObject = to.Specification.GitOpsParam.GitOpsReferenceObject
		gitOpsParams.GitOpsResourceObject = to.Specification.GitOpsParam.GitOpsResourceObject

		m[ClusterSyncResource] = gitOpsParams.GitOpsReferenceObject
		clustersync_manager := GetManagerset().ClusterSync
		clustersync_obj, err := clustersync_manager.GetObject(m)
		if clustersync_obj.GetMetadata().Name != gitOpsParams.GitOpsReferenceObject || err != nil {
			log.Println("error finding clustersync object", err)
			return &module.HubObject{}, err
		}

		if gitOpsParams.GitOpsResourceObject != "" {
			m[ClusterSyncResource] = gitOpsParams.GitOpsResourceObject
			clustersync_obj, err := clustersync_manager.GetObject(m)
			if clustersync_obj.GetMetadata().Name != gitOpsParams.GitOpsResourceObject || err != nil {
				log.Println("error finding clustersync object", err)
				return &module.HubObject{}, err
			}
		}

		to.Status.Ip = to.Specification.PublicIps[0]
		err = GetDBUtils().RegisterGitOpsDevice(overlay_name, to.Metadata.Name, mtypes.GitOpsSpec{Props: gitOpsParams})
		if err != nil {
			log.Println(err)
		}
	} else {
		config, err := base64.StdEncoding.DecodeString(to.Specification.KubeConfig)
		if err != nil {
			log.Println(err)
			return t, err
		}

		//Stating the hub is using IPv6 or IPv4(default using IPv4)
		var local_public_ips, local_public_ip6s []string
		flag := true
		local_public_ips = to.Specification.PublicIps

		if len(local_public_ips) == 0 {
			flag = false
			local_public_ip6s = to.Specification.PublicIp6s
		}

		kubeutil := GetKubeConfigUtil()
		config, local_public_ip, err = kubeutil.checkKubeConfigAvail(config, local_public_ips, local_public_ip6s, DEFAULTPORT)
		if err == nil {
			log.Println("Public IP address verified: " + local_public_ip)
			if flag {
				to.Status.Ip = local_public_ip
			}
			to.Status.Ip6 = local_public_ip
			to.Specification.KubeConfig = base64.StdEncoding.EncodeToString(config)
			err := GetDBUtils().RegisterDevice(overlay_name, hub_name, to.Specification.KubeConfig)
			if err != nil {
				log.Println(err)
			}
		} else {
			return t, err
		}
	}

	//Store hub in database
	t, err := GetDBUtils().CreateObject(c, m, t)

	//Create cert for ipsec connection
	log.Println("Create Certificate: " + to.GetCertName())
	_, cert, key, err := certManager.GetOrCreateCertificateByType(overlay_name, to.Metadata.Name, HubKey, to.IsSecurityEnhanced())
	if err != nil {
		return t, err
	}

	if to.IsSecurityEnhanced() {
		go processSecurityMeasures(t, HubKey, overlay_name, key, cert, chann)
	} else {
		chann <- ConnectorInfo{t, HubKey}
	}

	return t, err
}

func (c *HubObjectManager) GetObject(m map[string]string) (module.ControllerObject, error) {
	// DB Operation
	t, err := GetDBUtils().GetObject(c, m)

	return t, err
}

func (c *HubObjectManager) GetObjects(m map[string]string) ([]module.ControllerObject, error) {
	// DB Operation
	t, err := GetDBUtils().GetObjects(c, m)

	return t, err
}

func (c *HubObjectManager) UpdateObject(m map[string]string, t module.ControllerObject) (module.ControllerObject, error) {
	// DB Operation
	t, err := GetDBUtils().UpdateObject(c, m, t)

	return t, err
}

func (c *HubObjectManager) DeleteObject(m map[string]string) error {
	//Check resource exists

	t, err := c.GetObject(m)
	if err != nil {
		return nil
	}

	overlay_manager := GetManagerset().Overlay
	certManager := GetManagerset().Cert

	// Reset all IpSec connection setup by this device
	err = overlay_manager.DeleteConnections(m, t)
	if err != nil {
		log.Println(err)
	}

	to := t.(*module.HubObject)

	// Remove security enhanced ops for device
	if to.IsSecurityEnhanced() {
		removeSecurityMeasures(t, m[OverlayResource])
	}

	log.Println("Delete Certificate: " + to.GetCertName())
	err = certManager.DeleteCertificateByType(m[OverlayResource], to.Metadata.Name, HubKey)
	if err != nil {
		log.Println("Error in deleting hub certificate")
	}

	if to.Specification.KubeConfig == "" {
		err = GetDBUtils().UnregisterGitOpsDevice(m[OverlayResource], to.Metadata.Name)
	} else {
		err = GetDBUtils().UnregisterDevice(m[OverlayResource], m[HubResource])
	}
	if err != nil {
		log.Println(err)
	}

	// DB Operation
	err = GetDBUtils().DeleteObject(c, m)
	if err != nil {
		log.Println(err)
	}

	return err
}

func (c *HubObjectManager) SetupConns(m map[string]string, obj module.ControllerObject) error {
	mainFlag, _, err := c.checkIPMode(obj, "")
	if err != nil {
		log.Println("Error checking ip")
		return err
	}

	//Get all hubs
	hubs, err := c.GetObjects(m)
	if err != nil {
		log.Println(err)
		return err
	}

	overlay := GetManagerset().Overlay
	if len(hubs) > 0 {
		for i := 0; i < len(hubs); i++ {
			if hubs[i].GetMetadata().Name == obj.GetMetadata().Name {
				continue
			}

			flag, _, _ := c.checkIPMode(hubs[i], "")
			if flag == mainFlag {
				err := overlay.SetupConnection(m, obj, hubs[i], HUBTOHUB, NameSpaceName, false)
				if !pkgerrors.Is(err, ConnectionExistErr) && err != nil {
					log.Println("Setup connection with " + hubs[i].(*module.HubObject).Metadata.Name + " failed.")
					return err
				}
			}
		}
	}

	_, err = GetDBUtils().UpdateObject(c, m, obj)
	if err != nil {
		return err
	}

	return nil

}

//Check whether the device is using the same mode as the variable mode
//If the mode is "", then return true for IPv4, return false for IPv6
func (c *HubObjectManager) checkIPMode(obj module.ControllerObject, mode string) (bool, string, error) {
        var flag bool
        var err error
	var ip string

	o := obj.(*module.HubObject)
        if o.Status.Ip != "" {
                flag = true
		ip = o.Status.Ip
	} else if o.Status.Ip6 != "" {
                flag = false
		ip = o.Status.Ip6
        } else {
                err = pkgerrors.New("No IP set for device, cannot judge IP mode")
        }

        if mode == "" {
                return flag, ip, err
        }

        switch mode {
        case IPV4:
                return flag && true, "", err
        case IPV6:
                return flag && false, "", err
        }

        return false, "", pkgerrors.New("Unknown IP mode for comparison")
}

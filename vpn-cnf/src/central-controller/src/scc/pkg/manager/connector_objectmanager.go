/*
 * Copyright 2020 Intel Corporation, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package manager

import (
	"github.com/akraino-edge-stack/icn-sdwan/central-controller/src/scc/pkg/module"
)

// ControllerManager is an interface exposes the ControllerObject functionality
type ConnectorObjectManager interface {
	SetupConns(m map[string]string, obj module.ControllerObject) error
	checkIPmode(obj module.ControllerObject, mode string) bool
}

type ConnectorInfo struct {
        Object  module.ControllerObject
        ObjType string
}

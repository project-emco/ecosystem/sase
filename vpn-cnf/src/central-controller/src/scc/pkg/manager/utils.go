/*
 * Copyright 2021 Intel Corporation, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package manager

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"strings"
	"time"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	tciv1alpha1 "github.com/intel/trusted-certificate-issuer/api/v1alpha1"

	"github.com/akraino-edge-stack/icn-sdwan/central-controller/src/scc/pkg/module"
	"github.com/akraino-edge-stack/icn-sdwan/central-controller/src/scc/pkg/resource"
)

const (
	defaultKeyPath        = "/tmp/key/"
	defaultWrapBinaryPath = "/tmp/km-tools/km-wrap"
	defaultModule         = "/usr/lib/x86_64-linux-gnu/softhsm/libsofthsm2.so"

	defaultSignerName = "issuer.certmanager.io/default"
	defaultSecretType = "Opaque"
	customSecretType  = "KMRA"
	envkey = "SECURITY_ENHANCED"

	maxRand          = 0x7fffffffffffffff
	smartCardPrefix  = "%smartcard:"
	defaultKeyParams = "rsa:2048"
)

func format_resource_name(name1 string, name2 string) string {
	name1 = strings.Replace(name1, "-", "", -1)
	name2 = strings.Replace(name2, "-", "", -1)

	return strings.ToLower(name1 + name2)
}

func format_ip_as_suffix(ip string) string {
	var res string
	if strings.Contains(ip, ".") {
		res = "_" + strings.Replace(ip, ".", "", -1)
	} else {
		res = "_" + strings.Replace(ip, ":", "", -1)
	}
	return res
}

func GetConnectionCertificates(overlay_name, obj_name, obj_type string, security_flag bool) (string, string, string, string, error) {
	var priv_key, cert, ca_chain, subject string

	cert_manager := GetManagerset().Cert
	cert_name := cert_manager.GetCertName(obj_name, obj_type)
	subject = "/CN=" + cert_name
	ca_chain, cert, priv_key, err := cert_manager.GetOrCreateCertificateByType(overlay_name, obj_name, obj_type, security_flag)
	if err != nil {
		log.Println("Error in fetching certificates for " + obj_name)
		return "", "", "", "", err
	}

	if security_flag {
		id := generateUniqueKeyId()
		key := smartCardPrefix + id + " " + defaultKeyParams
		priv_key = b64.StdEncoding.EncodeToString([]byte(key))
		cert = b64.StdEncoding.EncodeToString([]byte(""))
		subject = "/CN=" + cert_name + "-" + id
	}

	return ca_chain, cert, priv_key, subject, nil
}

func generateUniqueKeyId() string {
	ra := rand.New(rand.NewSource(time.Now().UnixNano()))
	rn := ra.Int63n(maxRand)
	id := fmt.Sprintf("%v", rn)
	return id
}

func handleKeyWrapping(val, instanceName, key string) ([]byte, error) {
	spec, _, _, err := parseQuoteAttestation(val, false)
	if err != nil {
		return nil, err
	}

	if string(spec.ServiceID) != instanceName {
		return nil, fmt.Errorf("Unmatched identity in quoteattestation CR")
	}

	if _, err := os.Stat(defaultKeyPath); err != nil {
		fmt.Println("Creating tmp folders for key")
		err = os.MkdirAll(defaultKeyPath, os.ModeDir)
		if err != nil {
			fmt.Println("Failed to create tmp folder for key cert")
			return nil, err
		}
	}

	keyFile, err := os.CreateTemp(defaultKeyPath, "km-key-")
	if err != nil {
		return nil, fmt.Errorf("quote validation internal error: failed to create tmp file: %v", err)
	}

	defer func() {
		fileName := keyFile.Name()
		keyFile.Close()
		os.Remove(fileName)
	}()

	if _, err := keyFile.Write([]byte(key)); err != nil {
		return nil, fmt.Errorf("quote attestation internal error: failed to write to tmp file: %v", err)
	}

	wpkFile, err := os.CreateTemp(defaultKeyPath, "km-wpk-")
	if err != nil {
		return nil, fmt.Errorf("quote attestation internal error: failed to create tmp file: %v", err)
	}

	defer func() {
		fileName := wpkFile.Name()
		wpkFile.Close()
		os.Remove(fileName)
	}()

	if _, err := wpkFile.Write(spec.PublicKey); err != nil {
		return nil, fmt.Errorf("quote validation internal error: failed to write to tmp file: %v", err)
	}

	kmWrapArgs := []string{
		"--pubkey", wpkFile.Name(),
		"--privkey", keyFile.Name(),
		"--module", defaultModule,
	}

	cmd := exec.Command(defaultWrapBinaryPath, kmWrapArgs...)
	log.Println("command executed:", cmd.String())
	kmWrapOut, err := cmd.CombinedOutput()
	if err != nil {
		log.Printf("kmWrap command finished with error: %v", err)
		return nil, err
	}

	return kmWrapOut, nil
}

func queryQuoteAttestation(overlay, ns, crName, objKind, cid string, t module.ControllerObject) (string, error) {
	var to module.ControllerObject
	var val string
	var err error

	switch objKind {
	case "Hub":
		to = t.(*module.HubObject)
	case "Device":
		to = t.(*module.DeviceObject)
	case "SCC":
		to = &module.EmptyObject{Metadata: module.ObjectMetaData{"local", "", "", ""}}
	}

	if cid != "" {
		resutil := NewResUtil()
		resutil.qryCtxId = cid

		val, err = resutil.GetResourceData(to, ns, t.GetMetadata().Name + "-app", overlay)
		if err != nil {
			return "", err
		}
	}

	return val, nil
}

func parseQuoteAttestation(val string, containStatus bool) (resource.QuoteAttestationSpec, resource.QuoteAttestationStatus, metav1.ObjectMeta, error) {
	var vi interface{}
	var qaStatus resource.QuoteAttestationStatus
	var qaSpec resource.QuoteAttestationSpec
	var meta metav1.ObjectMeta

	err := json.Unmarshal([]byte(val), &vi)
	if err != nil {
		return qaSpec, qaStatus, meta, err
	}

	status := vi.(map[string]interface{})["resourceStatuses"]
        status_val, err := json.Marshal(status)

        var s []interface{}
        json.Unmarshal(status_val, &s)

        res := s[0].(map[string]interface{})["res"]
        res_val, err := json.Marshal(res)
        if err != nil {
                return qaSpec, qaStatus, meta, err
        }

        v, err := b64.StdEncoding.DecodeString(strings.Replace(string(res_val), "\"", "", -1))
        if err != nil {
                return qaSpec, qaStatus, meta, err
        }

	err = json.Unmarshal(v, &vi)
        if err != nil {
                return qaSpec, qaStatus, meta, err
        }

	metadata := vi.(map[string]interface{})["metadata"]
	metadata_val, err := json.Marshal(metadata)
	if err != nil {
		return qaSpec, qaStatus, meta, err
	}

	err = json.Unmarshal(metadata_val, &meta)
	if err != nil {
		return qaSpec, qaStatus, meta, err
	}

	if containStatus {
		status := vi.(map[string]interface{})["status"]
		status_val, err := json.Marshal(status)
		if err != nil {
			return qaSpec, qaStatus, meta, err
		}

		err = json.Unmarshal(status_val, &qaStatus)
		if err != nil {
			return qaSpec, qaStatus, meta, err
		}
	}

	spec := vi.(map[string]interface{})["spec"]
	spec_val, err := json.Marshal(spec)
	if err != nil {
		return qaSpec, qaStatus, meta, err
	}

	err = json.Unmarshal(spec_val, &qaSpec)
	if err != nil {
		return qaSpec, qaStatus, meta, err
	}

	return qaSpec, qaStatus, meta, nil
}

func watchQuoteAttestation(overlay, ns, crName, objKind string, t module.ControllerObject) (bool, error) {
	attested := false
	err := wait.Poll(10*time.Second, 60*time.Second, func() (bool, error) {
		val, err := queryQuoteAttestation(overlay, ns, crName, objKind, "", t)
		if err != nil {
			return false, fmt.Errorf("Error in query quote attestation cr")
		}

		_, status, _, err := parseQuoteAttestation(val, true)
		if err != nil {
			return false, err
		}

		var condition resource.QuoteAttestationCondition
		condition = status.Condition

		//ADDED: change to certain type matched with sgx-attestation-controller
		if string(condition.Type) == string(tciv1alpha1.ConditionReady) {
			attested = true
		}

		return attested, nil
	})

	if err == nil && attested == true {
		return true, nil
	}

	return false, err
}

func buildQuoteAttestationResourceFromVal(val, secret_name, secret_type string, attested bool) resource.QuoteAttestationResource {
	spec, status, meta, err := parseQuoteAttestation(val, attested)
	if err != nil {
		log.Println(err)
		return resource.QuoteAttestationResource{}
	}

	var res resource.QuoteAttestationResource
	var rawSpec resource.QuoteAttestationSpec
	var rawStatus resource.QuoteAttestationStatus
	var signerName = defaultSignerName

	if !strings.Contains(meta.Name, "-processed") {
		meta.Name = meta.Name + "-processed"
	}
	res.ObjectMeta = meta

	if spec.ServiceID != rawSpec.ServiceID {
		rawSpec.PublicKey = spec.PublicKey
		rawSpec.QuoteVersion = spec.QuoteVersion
		rawSpec.Quote = spec.Quote
		rawSpec.ServiceID = spec.ServiceID
		rawSpec.SignerNames = spec.SignerNames

		res.Spec = rawSpec
		if attested && status.Condition != rawStatus.Condition {
			signerName = spec.SignerNames[0]
		}
	}

	if attested {
		secrets := make(map[string]resource.QuoteAttestationSecret)
		secrets[signerName] = resource.QuoteAttestationSecret{
			SecretName: secret_name,
			SecretType: secret_type,
		}

		if status.Condition != rawStatus.Condition {
			rawStatus.Condition = status.Condition
		} else {
			condition := resource.QuoteAttestationCondition{
				Message:        "Not attested",
				State:          "Not attested",
				Type:           resource.AttestationFailed,
				LastUpdateTime: metav1.Now(),
			}
			rawStatus.Condition = condition
		}
		rawStatus.Secrets = secrets
		res.Status = rawStatus
	}

	return res
}

func processSecurityMeasures(t module.ControllerObject, kind, overlay_name, key, cert string, chann chan ConnectorInfo) {
	var wrappedData []byte
	rollback := false
	resutil := NewResUtil()
	name := t.GetMetadata().Name

	//create tcs issuer
	resutil.AddResource(t, "create", &resource.IssuerResource{
		ObjectMeta: metav1.ObjectMeta{
                                Name:      "issuer4" + name,
                                Namespace: "default",
                },
		Spec: resource.TCSIssuerSpec{
			SecretName: "quotesecret4" + name,
			SelfSignCertificate: false,
		},
	})
	cid, err := resutil.DeployNew(overlay_name, "issuer4"+name, "YAML", "")
	if err != nil {
		log.Println("Failed to deploy issuer to corresponding cluster")
		return
	}

	//fetch back the quoteattestation cr
	val, err := queryQuoteAttestation(overlay_name, "default", "issuer4"+name, kind, cid, t)
	if err != nil {
		log.Println("Failed to apply security enhancement.")
		rollback = true
	}


	if !rollback {
		//FIXED: Re-assign the quote attestation to overlay cluster
		quote_attestation := buildQuoteAttestationResourceFromVal(val, "", "", rollback)
		log.Println(quote_attestation.ToYaml("scc"))

		scc := module.EmptyObject{Metadata: module.ObjectMetaData{"local", "", "", ""}}
		resutil.AddResource(&scc, "create", &quote_attestation)
		resutil.Deploy(overlay_name, "quoteattestation4"+name, "YAML", "")

		//FIXED: Add condition when the status of Quote Attestation CR has changed to successful
		securityEnhanced := os.Getenv(envkey)
		if securityEnhanced == "false" {
			rollback = true
		} else if attest_res, err := watchQuoteAttestation(overlay_name, "default", "sgxquoteattestation-hsm-"+name+"-processed", "SCC", t); attest_res != true {
			log.Println("Failed to pass the attestation in time.")
			if err != nil {
				log.Println(err)
			}
			rollback = true
		}

		if !rollback {
			wrappedData, err = handleKeyWrapping(val, name, key)
			if err != nil {
				log.Println("Error in handling attestation and key wrapping: ", err)
				rollback = true
			}
		}
	}

	encoded_cert := b64.StdEncoding.EncodeToString([]byte(cert))
	secretData := make(map[string][]byte)
	if !rollback {
		secretData[v1.TLSPrivateKeyKey] = wrappedData
	} else {
		//if attestation failed, rollback to plain key
		encoded_key := b64.StdEncoding.EncodeToString([]byte(key))
		secretData[v1.TLSPrivateKeyKey] = []byte(encoded_key)
	}
	secretData[v1.TLSCertKey] = []byte(encoded_cert)

	if len(resutil.resmap) != 0 {
		resutil = NewResUtil()
	}
	resutil.AddResource(t, "create", &resource.SecretResource{
		v1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "quotesecret4" + name,
				Namespace: "default",
			},
			Type: defaultSecretType,
			Data: secretData,
		},
	})

	if !rollback {
		val, err = queryQuoteAttestation(overlay_name, "default", "sgxquoteattestation-hsm-"+name+"-processed", "SCC", "", t)
		if err != nil {
			log.Println("Failed to fetch the attested quote attestation cr.")
		}
	}

	//TODO: Re-deploy the quoteattestation cr back to hub
	quote_attestation := buildQuoteAttestationResourceFromVal(val, "quotesecret4"+name, customSecretType, true)
	resutil.AddResource(t, "create", &quote_attestation)
	resutil.Deploy(overlay_name, "quoteattestation4"+name, "YAML", "")

	// Add task to channel
	chann <- ConnectorInfo{t, kind}
}

func setupConnectors(overlay_name string, connectorChan chan ConnectorInfo) {
	for {
		cinfo, ok := <- connectorChan
		if !ok {
			return
		}

		ctype := cinfo.ObjType

		m := make(map[string]string)
		m[OverlayResource] = overlay_name

		switch ctype {
		case HubKey:
			hubManager := GetManagerset().Hub
			//Call Hub connector
			err := hubManager.SetupConns(m, cinfo.Object)
			if err != nil {
				log.Println("error in setup connections for hub", cinfo.Object.GetMetadata().Name)
				continue
			}
		case DeviceKey:
			deviceManager := GetManagerset().Device
			//Get all devices
			err := deviceManager.SetupConns(m, cinfo.Object)
			if err != nil {
				log.Println("error in setup connections for device", cinfo.Object.GetMetadata().Name)
				continue
			}
		}
	}
}

func removeSecurityMeasures(t module.ControllerObject, overlay_name string) {
	resutil := NewResUtil()
        name := t.GetMetadata().Name

        //remove tcs issuer
        resutil.AddResource(t, "create", &resource.IssuerResource{
                ObjectMeta: metav1.ObjectMeta{
                                Name:      "issuer4" + name,
                                Namespace: "default",
                },
                Spec: resource.TCSIssuerSpec{
                        SecretName: "quotesecret4" + name,
                        SelfSignCertificate: false,
                },
        })

	resutil.Undeploy(overlay_name)
}

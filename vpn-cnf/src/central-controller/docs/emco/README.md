# Integrate SDEWAN Overlay Controller with EMCO

The Edge Multi-Cluster Orchestrator (EMCO) is a software framework for intent-based deployment of cloud-native applications to a set of Kubernetes clusters, spanning enterprise data centers, multiple cloud service providers and numerous edge locations. It is architected to be flexible, modular and highly scalable. It is aimed at various verticals, including telecommunication service providers.

Refer to [EMCO documentation](https://gitlab.com/project-emco/core/emco-base/-/blob/main/docs/design/emco-design.md) for details on EMCO architecture.

## Pre-requirement

In general, to install and use EMCO, you will need at least **2 Kubernetes clusters**. One cluster to run the EMCO microservices themselves, and one to many Kubernetes clusters where the applications to be deployed by EMCO will reside.

Additionally, each of the Kubernetes where applications (and [Logical Clouds](https://gitlab.com/project-emco/core/emco-base/-/blob/main/docs/design/Logical_Clouds.md)) reside also require that the [EMCO Monitor](https://gitlab.com/project-emco/core/emco-base/-/blob/main/docs/design/monitor.md) (also known as EMCO Status Monitoring) service be running. Instructions to deploy Monitor are provided in this readme file.

Refer to the [Release Notes](https://gitlab.com/project-emco/core/emco-base/-/blob/main/ReleaseNotes.md) for a tested compatibility table between versions of EMCO and versions of Kubernetes, Helm, and others.

## EMCO Installation

To install `EMCO` to your cluster, you can refer to this [guide](https://gitlab.com/project-emco/core/emco-base#installation).

## Steps to integrate `SDEWAN Overlay Controller` with EMCO

**1.Apply the changes to use EMCO DB and `rsync`

```bash
# Navigate to sdewan project root directory
cd central-controller/docs/emco
git apply emco.patch
```

**2.Re-build the `scc` docker image**

```bash
# Navigate to sdewan project root directory
docker build -f build/Dockerfile . -t scc
```

**3.Create sdewan overlay controller in EMCO namespace**

```bash
# Navigate to sdewan project root directory
cd deployments/kubernetes
kubectl apply -f scc_secret.yaml -n emco
kubectl apply -f scc.yaml -n emco
```

**4.Verify the installation**

```bash
# Create overlay
curl --location --request POST 'http://<scc>:9015/scc/v1/overlays' \
--header 'Cache-Control: no-cache' \
--header 'Content-Type: application/json' \
--data-raw '{
    "metadata": {
        "name": "overlay1",
        "description": "",
        "userData1": "",
        "userData2": ""
    },
    "spec": {}
}'

# And then we can check the emco database to get the overlay register information
kubectl exec -ti -n emco emco-db-emco-mongo-0 bash

# Connect to mongo db
mongo -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD"

# Check the information in EMCO db
use emco
db.getCollection("centralcontroller").find().forEach(printjson)
```


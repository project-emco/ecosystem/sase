#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# Copyright (c) 2022 Intel Corporation

set -o errexit
set -o nounset
set -o pipefail

source _common.sh

function create_service {

   cat << NET > $WORKING_DIR/$name-service-data.yaml
name: $name
destinationHost: $destinationHost
destinationHostPort: $destinationHostPort
destinationHostPortInternal: $destinationHostPortInternal
address: $address
dns: $dns
NET
   gomplate -d data=$WORKING_DIR/$name-service-data.yaml -f ./istio/service-entry-template.yaml > $WORKING_DIR/$name-se-istio.yaml

    # Apply app istio resources including authorization
    apply_cluster   $WORKING_DIR/$name-se-istio.yaml
}

function delete_service {
    local name=$1
    delete_cluster $WORKING_DIR/$name-se-istio.yaml
    rm $WORKING_DIR/$name-se-istio.yaml
}


while getopts ":v:" flag
do
    case "${flag}" in
        v) values=${OPTARG}
           name=$(./yq eval '.name' $values)
           destinationHost=$(./yq eval '.host' $values)
           destinationHostPort=$(./yq eval '.port' $values)
           destinationHostPortInternal=$(./yq eval '.internal' $values)
           address=$(./yq eval '.address' $values)
           dns=$(./yq eval '.dns' $values) ;;
    esac
done
shift $((OPTIND-1))

WORKING_DIR=/tmp/
case "$1" in
    "create" )
        create_service
    ;;
    "delete" )
        delete_service
    ;;
    *)
    usage ;;
esac

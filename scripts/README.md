# Setup

![Demo Setup](./demosetup.png)

## Pre-requisites

Install Docker, Kubernetes, calico and istio in all Edge Clusters except Edge 5.
The cluster.sh script can be used for installing Docker, Kubernetes and calico,

To install docker
```
./cluster.sh install_docker
```
To install kubernetes
```
./cluster.sh install_kubernetes
```
To install calico
```
./cluster.sh install_calico
```

### Istio Installation

Download Istio,
```
curl -L https://istio.io/downloadIstio | sh -
```

Create configuration file like the example:
```
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
metadata:
  name: istiooperator-config
  namespace: istio-system
spec:
  profile: minimal
  meshConfig:
    accessLogFile: /dev/stdout
    enableAutoMtls: true
    defaultConfig:
      proxyMetadata:
        # Enable Istio agent to handle DNS
        ISTIO_META_DNS_CAPTURE: "true"
  components:
    # Enable Istio Ingress gateway
    ingressGateways:
    - name: istio-ingressgateway
      enabled: true
      k8s:
        env:
          - name: ISTIO_META_ROUTER_MODE
            value: "sni-dnat"
        service:
          type: NodePort
          ports:
            - port: 80
              targetPort: 8080
              name: http2
            - port: 443
              targetPort: 8443
              name: https
            - port: 15443
              targetPort: 15443
              name: tls
              nodePort: 32001
  values:
    global:
      pilotCertProvider: istiod
```
Install Istio using the above configuration file:
```
 istioctl install  -f istio-config.yaml
```

## Edge 1 and Edge 2
Edge 1 and Edge 2 clusters are used for running applications. Few examples of applications that can be installed are bookinfo and httpbin. Bookinfo application can be installed by following  https://istio.io/latest/docs/examples/bookinfo/ and httpbin application can be installed by following https://istio.io/latest/docs/tasks/traffic-management/ingress/ingress-control/#before-you-begin

## Edge 3 (POP Location)

Edge 3 is considerd as the POP location, the cluster.sh script is used to setup this cluster.

"prepare_pop" option is used to install helm and metallb, configure metallb, create "lbns" namespace, istio-ingressgateway and cert-manager.
```
./cluster.sh prepare_pop
```

"create_issuer" option is used create the keys and certificates.
```
./cluster.sh create_issuer
```

"power_dns" option is used to setup power dns
```
./cluster.sh power_dns
```

## Edge 4

Edge 4 is used as the keycloak idp and as a control cluster, the cluster.sh script is also used to setup this cluster.

"packages" option is used to install yq, gomplate and helm
```
./cluster.sh packages
```

"keycloak_idp" option is used to setup keycloak_idp
```
./cluster.sh keycloak_idp
```

"power_dns" option is used to setup power dns
```
./cluster.sh power_dns
```

After setting up the cluster, we can start adding customer, app, service and authorization.

### Add Customer

Create a values.yaml file like below.
More examples in the examples folder

```
name: customer1
namespace: c1ns
domain: customer1.com
idp: 192.168.121.11:30232

```
To create a customer configutation:
```

./addCustomer.sh -v examples/customer1/customer.yaml create

```

This step perfoms following steps:

- Add Gateway and VirtualService resource for the outer Istio Ingress Proxy (LB)
- Create namespace for the customer
- Deploy keycloak broker for the customer
- Deploy Istio Ingress gateway in the new namespace
- Deploy oauth2-proxy in the new namespace
- Configure oauth2-proxy
- Configure Keycloak
- Edit Istio configmap to add customer oauth2-proxy as an extensionProvider


### Add Application for the customer

```
name: customer1
namespace: c1ns
domain: customer1.com
app: app1
host: app1.internalcustomer1.com
port: 8000

```

```

./addApp.sh -v examples/customer1/app1.yaml create

```

### Add Service

For each application deployed on user clusters add Service to provide connectivity info

```
name: app1
host: app1.internalcustomer1.com
port: 31166
internal: 8000
address: 240.0.0.2

```

```

./addService.sh -v examples/service.yaml create

```

### Add Authentication for roles

```
name: customer1
namespace: c1ns
# this is used for internal purposes only
# must be unique per role
index: 1
app: app1
role: general
url: /productpage

```

```

./addAppAuthz.sh -v examples/customer1/role1.yaml create

```

### CIDR assignment

1) The CIDR for each customer/namespace should be unique and not overlap. For example if the CIDR for the POP is 10.233.0.0/16 then for customer1/c1ns the CIDR can be 10.233.111.0/24 and for customer2/c2ns the CIDR can be 10.233.222.0/24.
2) The service CIDR for each Data Centre(dc) and customer combo should be unique and a subset of the service CIDR assigned for the customer. For example if the service CIDR for customer2 is 10.128.0.0/12 then the service CIDR for dc1 can be 10.128.0.0/16 and for dc2 can be 10.129.0.0/16.

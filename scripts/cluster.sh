#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# Copyright (c) 2022 Intel Corporation

set -o errexit
set -o nounset
set -o pipefail

source _common.sh

function install_yq_locally {
    if [ ! -x ./yq ]; then
        echo 'Installing yq locally'
        VERSION=v4.12.0
        BINARY=yq_linux_amd64
        sudo wget https://github.com/mikefarah/yq/releases/download/${VERSION}/${BINARY} -O yq && sudo chmod +x yq
fi
}

WORKING_DIR=/tmp

function install_keycloak_idp {

   kubectl create cm -n default keycloak-configmap --from-file=keycloak/realm_idp.json -o yaml --dry-run=client > $WORKING_DIR/keycloak-cm.yaml
   cat << NET > $WORKING_DIR/data.yaml
namespace: default
NET
   gomplate -d data=$WORKING_DIR/data.yaml -f ./keycloak/keycloak.yaml > $WORKING_DIR/keycloak.yaml
   #Install Keycloak cm
   kubectl apply -f   $WORKING_DIR/keycloak-cm.yaml
   #Install Keycloak
   kubectl apply -f   $WORKING_DIR/keycloak.yaml
}

function install_docker {
   sudo apt-get update
   sudo apt-get install docker.io
   sudo systemctl enable docker
   sudo systemctl start docker
}

function configure_metallb_l2 {

read -p 'Number of Ip Pools : ' n

   for((i=1; i<=$n; i++)); do
    read -p "Enter $i ip pool Name : " test[i]
    read -p 'IPAddr1: ' ipAddr1
    read -p 'IPAddr2: ' ipAddr2
      cat << NET > ${test[$i]}.yaml
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: ${test[$i]}
  namespace: metallb-system
spec:
  addresses:
  - $ipAddr1-$ipAddr2

NET

kubectl apply -f ${test[$i]}.yaml
echo "===== $i IP Address Pool Created ====="

   done

echo "===== Applying L2 Adveritsement ====="
cat << NET > l2advertisement.yaml
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: example
  namespace: metallb-system
NET

kubectl apply -f l2advertisement.yaml

echo "===== L2 Adveritsement created ====="
}

function configure_metallb_bgp {

echo "===== Configure metallb with BGP ====="
read -p 'BGP router IP address : ' bgpIP
read -p 'BGP router ASN : ' bgpASN
read -p 'Metallb ASN : ' metallbASN
cat << NET > bgp_peer.yaml
apiVersion: metallb.io/v1beta2
kind: BGPPeer
metadata:
  name: sample
  namespace: metallb-system
spec:
  myASN: $metallbASN
  peerASN: $bgpASN
  peerAddress: $bgpIP
NET

kubectl apply -f bgp_peer.yaml

echo "===== Metallb configured with BGP ====="


read -p 'Number of Ip Pools : ' n

   for((i=1; i<=$n; i++)); do
    read -p "Enter $i ip pool Name : " test[i]
    read -p 'IPAddr1: ' ipAddr1
    read -p 'IPAddr2: ' ipAddr2
      cat << NET > ${test[$i]}.yaml
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: ${test[$i]}
  namespace: metallb-system
spec:
  addresses:
  - $ipAddr1-$ipAddr2

NET

kubectl apply -f ${test[$i]}.yaml
echo "===== $i IP Address Pool Created ====="

   done

echo "===== Applying BGP Adveritsement ====="
cat << NET > bgpadvertisement.yaml
apiVersion: metallb.io/v1beta1
kind: BGPAdvertisement
metadata:
  name: example
  namespace: metallb-system

NET

kubectl apply -f bgpadvertisement.yaml

echo "===== BGP Adveritsement created ====="
}

function configure_metallb_l2_demo {

      cat << NET > first-pool.yaml
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: first-pool
  namespace: metallb-system
spec:
  addresses:
  - 10.10.10.19-10.10.10.20

NET

      cat << NET > second-pool.yaml
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: second-pool
  namespace: metallb-system
spec:
  addresses:
  - 192.168.121.27-192.168.121.29

NET


kubectl apply -f first-pool.yaml
kubectl apply -f second-pool.yaml
echo "=====  IP Address Pools Created ====="

echo "===== Applying L2 Adveritsement ====="
cat << NET > l2advertisement.yaml
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: example
  namespace: metallb-system
NET

kubectl apply -f l2advertisement.yaml

echo "===== L2 Adveritsement created ====="
}

function setup_bird_bgp {

   sudo apt-get update
   sudo apt-get -y install bird-bgp

   read -p "Enter BGP router IP address : " ipAddrBGP
   read -p "Enter metallb VM IP address : " ipAddrMetallb
   read -p "BIRD BGP ASN : " bgpASN
   read -p "Metallb ASN : " metallbASN

   cat << NET > /etc/bird/bird.conf
log syslog all;

# Turn on global debugging of all protocols (all messages or just selected classes)
debug protocols all;
debug protocols { events, states };

router id $ipAddrBGP;

# The Kernel protocol is not a real routing protocol. Instead of communicating
# with other routers in the network, it performs synchronization of BIRD's
# routing tables with the OS kernel.
protocol kernel {
        persist;
        scan time 60;
        export all;   # Actually insert routes into the kernel routing table
}

# The Device protocol is not a real routing protocol. It doesn't generate any
# routes and it only serves as a module for getting information about network
# interfaces from the kernel.
protocol device {
        scan time 30;
}

protocol bgp metallb {
        import all;
        export all;

        local as $bgpASN;
        neighbor $ipAddrMetallb as $metallbASN;
        multihop;
        passive;
}
NET

echo "===== Configuring bird BGP router ====="

sudo birdc configure

echo "===== Bird BGP router installed ====="

}

function install_cluster_packages {

  install_yq_locally
  sudo wget https://github.com/hairyhenderson/gomplate/releases/download/v3.11.2/gomplate_linux-amd64
  sudo mv gomplate_linux-amd64 /usr/local/bin/gomplate
  sudo chmod +x /usr/local/bin/gomplate
  sudo apt-get install jq

   install_helm

   # add oauth2
   helm repo add oauth2-proxy https://oauth2-proxy.github.io/manifests
   helm repo update

}

function prepare_pop {

   install_helm

   if [[ $(kubectl get ns lbns)  ]]; then
      echo "Namespace lbns exists"
   else
      kubectl create ns lbns
      echo "Namespace lbns created"
   fi

   echo "==== Install Metallb ===="
   install_metallb
   echo "==== Metallb Installed ===="

   echo "==== Configure Metallb ===="
   configure_metallb_bgp
   echo "==== Metallb Configured ===="


   echo "==== Install Istio-Ingressgateway ===="
   helm repo add istio https://istio-release.storage.googleapis.com/charts
   helm repo update
   helm install istio-ingressgateway-lb -n lbns --set annotations."metallb\.universe\.tf/address-pool"="second-pool" istio/gateway
   echo "==== Istio-Ingressgateway Installed ==== "

   kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml
   kubectl apply -f ./controllers/kncc.yaml

   sudo modprobe ip_gre
   sudo modprobe nf_conntrack_proto_gre

}

function prepare_pop_demo {

   install_helm

   if [[ $(kubectl get ns lbns)  ]]; then
      echo "Namespace lbns exists"
   else
      kubectl create ns lbns
      echo "Namespace lbns created"
   fi

   echo "==== Install Metallb ===="
   install_metallb
   echo "==== Metallb Installed ===="

   sleep 20
   echo "==== Configure Metallb ===="
   configure_metallb_l2_demo
   echo "==== Metallb Configured ===="


   echo "==== Install Istio-Ingressgateway ===="
   helm repo add istio https://istio-release.storage.googleapis.com/charts
   helm repo update
   helm install istio-ingressgateway-lb -n lbns --set annotations."metallb\.universe\.tf/address-pool"="second-pool"  istio/gateway
   echo "==== Istio-Ingressgateway Installed ==== "

   kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml
   kubectl apply -f ./controllers/kncc.yaml

}



function install_metallb {

kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml

}

function install_helm {

   echo "===== Installing Helm ====="
   sudo wget https://get.helm.sh/helm-v3.9.1-linux-amd64.tar.gz
   tar -zxvf helm-v3.9.1-linux-amd64.tar.gz
   sudo mv linux-amd64/helm /usr/local/bin/helm
   echo "===== Helm installed ====="

}

function install_kubernetes {
   kubernetes_version="1.23.1"
   read -p 'Pod Network cidr : ' pod_network_cidr
   read -p 'Service cidr : ' svc_cidr
   read -p 'Node Name : ' node_name

   sudo apt-get update -y
   sudo apt-get install apt-transport-https -y
   sudo apt-get install ca-certificates -y
   sudo apt-get install curl -y
   sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
   echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] \
      https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

   sudo apt-get update -y


   sudo apt-get install kubectl=1.23.0-00 -y #install kubectl first
   sudo apt-get install kubelet=1.23.0-00 -y
   sudo apt-get install kubeadm=1.23.0-00 -y
   sudo apt-mark hold kubelet kubeadm kubectl
   sudo systemctl stop kubelet

   cgroup=$(sudo docker info | grep -i "cgroup driver" | cut -d ':' -f 2)

   echo "====cgroup : $cgroup=========="

   sudo echo "Environment="KUBELET_EXTRA_ARGS=--cgroup-driver=cgroupfs"" |sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
   sudo systemctl daemon-reload
   sudo sleep 5
   sudo apt-get upgrade -y

   if [ -z "$svc_cidr" ]
   then
         sudo kubeadm init --node-name=$node_name --kubernetes-version=$kubernetes_version --pod-network-cidr=$pod_network_cidr
   else
         sudo kubeadm init --node-name=$node_name --kubernetes-version=$kubernetes_version --pod-network-cidr=$pod_network_cidr --service-cidr=$svc_cidr
   fi

   sudo sleep 5

   sudo mkdir -p $HOME/.kube
   sudo cp -f /etc/kubernetes/admin.conf $HOME/.kube/config
   sudo ls -l /etc/kubernetes/
   sudo ls -l $HOME/.kube/

   export KUBECONFIG=$HOME/.kube/config
   sudo chown $(id -u):$(id -g) $HOME/.kube/config

   echo "get nodes"

   kubectl get node

   sudo sleep 5

   kubectl taint node $node_name node-role.kubernetes.io/master:NoSchedule-

   echo "done"
}

function install_calico {
   read -p 'Default Network cidr : ' cidr
   kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.3/manifests/tigera-operator.yaml
   #kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.3/manifests/custom-resources.yaml
   cat << NET > $WORKING_DIR/calico-default.yaml
apiVersion: operator.tigera.io/v1
kind: Installation
metadata:
  name: default
spec:
  # Configures Calico networking.
  calicoNetwork:
    # Note: The ipPools section cannot be modified post-install.
    ipPools:
    - blockSize: 26
      cidr: $cidr
      encapsulation: VXLANCrossSubnet
      natOutgoing: Enabled
      nodeSelector: all()

---
apiVersion: operator.tigera.io/v1
kind: APIServer
metadata:
  name: default
spec: {}

NET
  kubectl apply -f $WORKING_DIR/calico-default.yaml

}

function create_issuer {
   #Create cluster wide issuer
   sudo openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 -subj '/O=myorg/CN=myorg' -keyout ca.key -out ca.crt
   sudo kubectl create secret tls my-ca --key ca.key --cert ca.crt -n cert-manager
   kubectl apply -f ./certs/clusterissuer.yaml
}


function setup_powerdns {
   read -p 'Pod Network cidr : ' pod_network_cidr
   sudo systemctl disable --now systemd-resolved
   cat <<NET > /etc/resolv.conf
nameserver 10.248.2.1,
nameserver 10.3.86.124
NET
sudo apt-get update
sudo apt install pdns-server
sudo apt-get install pdns-backend-sqlite3
cat <<NET > /etc/powerdns/pdns.conf
    api=yes
    api-key=<api_key: NETSLICING>
    log-dns-queries=yes
    loglevel=9
    webserver-address=0.0.0.0
    webserver-allow-from=$pod_network_cidr
    launch=gsqlite3
    gsqlite3-database=/powerdns/pdns.sqlite3
NET
sudo systemctl restart pdns.service
sudo apt-get install sqlite3
cd / && mkdir powerdns
sudo cp /var/lib/powerdns/pdns.sqlite3 ./powerdns
sudo chown -R pdns:pdns /powerdns
sudo chmod 777 ./powerdns/pdns.sqlite3
sudo systemctl restart pdns.service
}

function setup_vlan {

read -p 'Adapter name : ' adapter_name
read -p 'vlan tag : ' vlan_tag
read -p 'ip cidr : ' ip_cidr


sudo ip link add link $adapter_name name $adapter_name"."$vlan_tag type vlan id $vlan_tag
sudo ip addr add $ip_cidr dev $adapter_name"."$vlan_tag
sudo ip link set dev $adapter_name"."$vlan_tag up

}

function setup_vpn_gateway {

kubectl create namespace sdewan-system
kubectl apply -f ../vpn-cnf/helm/cert/

helm package ../vpn-cnf/helm/sdewan_cnf
helm install ./cnf-0.1.0.tgz --generate-name

helm package ../vpn-cnf/helm/sdewan_controllers
helm install ./controllers-0.1.0.tgz --generate-name


}


case "$1" in
      "install_kubernetes" )
      install_kubernetes;;
      "install_calico" )
      install_calico;;
      "install_docker" )
      install_docker;;
     "prepare_pop" )
        prepare_pop;;
     "prepare_pop_demo" )
        prepare_pop_demo;;
      "create_issuer" )
      create_issuer;;
      "setup_powerdns" )
      setup_powerdns;;
      "packages" )
        install_cluster_packages;;
      "bird_bgp" )
        setup_bird_bgp;;
      "setup_vlan" )
        setup_vlan;;
      "setup_vpn_gateway" )
        setup_vpn_gateway;;
      "keycloak_idp" )
      install_keycloak_idp

esac

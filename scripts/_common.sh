#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# Copyright (c) 2020 Intel Corporation

set -o errexit
set -o nounset
set -o pipefail

#Change this to point to the correct config
KUBE_PATH=config
WORKING_DIR=/tmp

function apply_cluster {
    local file=$1
    echo "Applying to cluster: $file"
    kubectl apply -f $file --kubeconfig $KUBE_PATH
}

function apply_cluster_namespace {
    local file=$1
    local namespace=$2
    echo "Applying to cluster: $file"
    kubectl apply -f $file -n $namespace --kubeconfig $KUBE_PATH
}

function delete_cluster {
    local file=$1
    echo "Deleting from cluster: $file"
    kubectl delete -f $file --kubeconfig $KUBE_PATH
}

function delete_cluster_namespace {
    local file=$1
    local namespace=$2
    echo "Deleting from cluster: $file"
    kubectl delete -f $file -n $namespace --kubeconfig $KUBE_PATH
}

function create_namespace {
    local namespace=$1
    local name=$2
    if [[ $(kubectl get ns $namespace --kubeconfig $KUBE_PATH)  ]]; then
      echo "Namespace $namespace exists"
   else
      kubectl create ns $namespace --kubeconfig $KUBE_PATH
      echo "Namespace $namespace created"
      kubectl annotate namespace $namespace cni.projectcalico.org/ipv4pools=[\"$name\"] --kubeconfig $KUBE_PATH
   fi
}

function get_lb_ip {
    lbns_ip=$(kubectl get svc -n lbns --kubeconfig $KUBE_PATH | grep istio-ingressgateway-lb | awk '{print $4}')
    echo $lbns_ip
}


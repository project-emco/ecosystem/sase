#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# Copyright (c) 2022 Intel Corporation

set -o errexit
set -o nounset
set -o pipefail

source _common.sh

name="oops"
namespace="oops"
# list of comma seprated values
domain_list="oops"
app_name="oops"
destination_host="oops"

function create_app {
    echo $domain_list
   IFS=', ' read -r -a array <<< "$domain_list"
   domain1="${array[0]}"
   domain2="${array[1]}"
   appDomainName=$app_name.$domain1
   appDomain2Name=$app_name.$domain2
   cat << NET > $WORKING_DIR/$app_name-data.yaml
namespace: $namespace
customerName: $name
caCommonName: $name
appName: $app_name
appDomainName: $appDomainName
appDomain2Name: $appDomain2Name
destinationHost: $destination_host
NET
   gomplate -d data=$WORKING_DIR/$app_name-data.yaml -f ./certs/cert-template.yaml > $WORKING_DIR/$app_name-cert.yaml
   gomplate -d data=$WORKING_DIR/$app_name-data.yaml -f ./istio/app-gateway-vs-template.yaml > $WORKING_DIR/$app_name-istio.yaml

    #Create cert  for the app
    apply_cluster   $WORKING_DIR/$app_name-cert.yaml
    # Apply app istio resources including authorization
    apply_cluster   $WORKING_DIR/$app_name-istio.yaml
}

function delete_app {
    delete_cluster $WORKING_DIR/$app_name-istio.yaml
    delete_cluster $WORKING_DIR/$app_name-cert.yaml
    rm $WORKING_DIR/$app_name-*.yaml
}

while getopts ":v:" flag
do
    case "${flag}" in
        v) values=${OPTARG}
           name=$(./yq eval '.name' $values)
           namespace=$(./yq eval '.namespace' $values)
           domain_list=$(./yq eval '.domain' $values)
           app_name=$(./yq eval '.app' $values)
           destination_host=$(./yq eval '.host' $values);;
    esac
done
shift $((OPTIND-1))

WORKING_DIR=/tmp/$name
case "$1" in
    "create" )
        create_app
        rm $WORKING_DIR/$app_name-data.yaml
    ;;
    "delete" )
        delete_app
    ;;
    *)
    usage ;;
esac

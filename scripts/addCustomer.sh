#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# Copyright (c) 2022 Intel Corporation

set -o errexit
set -o nounset
set -o pipefail

source _common.sh

function  generate_data {

    IFS=', ' read -r -a array <<< "$domain_list"
    domain1="${array[0]}"
    domain2="${array[1]}"
    http_domain=".$domain1"
    https_domain=".$domain1"
    http_domain2=".$domain2"
    https_domain2=".$domain2"


    token_url="http://$idp_server/realms/users/protocol/openid-connect/token"
    authorization_url="http://$idp_server/realms/users/protocol/openid-connect/auth"
    ## TODO: Remove wildcard redirect URL by add per app/per domain redirect url
    #jq '.realm = '\"$name\"' | .clients[].redirectUris[0] = '\"$http\"' | .clients[].redirectUris[1] = '\"$https\"' | .clients[].redirectUris[2] = '\"$http\"' | .clients[].redirectUris[3] = '\"$https1\"' | .clients[].redirectUris[4] = '\"*\"' | .identityProviders[].config.tokenUrl = '\"$token_url\"' | .identityProviders[].config.authorizationUrl = '\"$authorization_url\"''  keycloak/realm.json  > $WORKING_DIR/realm.json
    jq '.realm = '\"$name\"' |  .clients[].redirectUris[0] = '\"*\"' | .identityProviders[].config.tokenUrl = '\"$token_url\"' | .identityProviders[].config.authorizationUrl = '\"$authorization_url\"''  keycloak/realm.json  > $WORKING_DIR/realm.json
    whitelistDomains=.$domain1:*
    whitelistDomain2=.$domain2:*
    #redirectUrl="https://$domain1:$https_port/oauth2/callback"
    redirectUrl="/oauth2/callback"
    istioHosts='"'*.$domain1'"'
    domain2Hosts='"'*.$domain2'"'
    authDomainName=authentication.$domain1
    authDomainName2=authentication.$domain2
    cat << NET > $WORKING_DIR/data.yaml
namespace: $namespace
customerName: $name
domainName: $domain1
domain2: $domain2Hosts
domain2Name: $domain2
whitelistDomains: $whitelistDomains
redirectUrl: $redirectUrl
istioHosts: $istioHosts
whitelistDomain2: $whitelistDomain2
https_domain: $https_domain
https_domain2: $https_domain2
authDomainName: $authDomainName
authDomainName2: $authDomainName2
cidr: $cidr
greTableName: $greTableName
greServiceCIDR: $greServiceCIDR
NET

}

function generate_oauth2_data {
    clientID="oauth2-proxy"
    # Create unique name
    IFS=', ' read -r -a array <<< "$domain_list"
    domain1="${array[0]}"
    domain2="${array[1]}"
    oauthIssuerUrl="https://authentication.$domain1/realms/$name"
    oidcIssuerUrl="http://keycloak.$namespace.svc.cluster.local/realms/$name"
    redeemUrl="$oidcIssuerUrl/protocol/openid-connect/token"
    jwksUri="$oidcIssuerUrl/protocol/openid-connect/certs"
    loginUrl="$oauthIssuerUrl/protocol/openid-connect/auth"
    cat << NET >> $WORKING_DIR/data.yaml
clientID: $clientID
oidcIssuerUrl: $oidcIssuerUrl
redeemUrl: $redeemUrl
loginUrl: $loginUrl
jwksUri: $jwksUri
clientSecret: "lsuaCKsXRCQ0gID8BZHYK8tfAMlxP1cR"
cookieSecret: "UmRaMTlQajM1a2ordWFYRnlJb2tjWEd2MVpCK2grOFM="
oauthIssuerUrl: $oauthIssuerUrl
NET

}

function create_packages {
    create_namespace $namespace $name

    echo "install_calico_ippool"
    gomplate -d data=$WORKING_DIR/data.yaml -f ./network/calico.yaml > $WORKING_DIR/calico.yaml
    apply_cluster $WORKING_DIR/calico.yaml

    # Create yamls for ca-issuer, istio-gateway and keycloak
    gomplate -d data=$WORKING_DIR/data.yaml -f ./certs/ca-template.yaml > $WORKING_DIR/ca-issuer.yaml
    cp ./helm/gateway/values.yaml $WORKING_DIR/values.yaml
    echo -e "\nsdewanpurpose: $name"  >> $WORKING_DIR/values.yaml
    helm template istio-ingressgateway-$name -n $namespace -f $WORKING_DIR/values.yaml ./helm/gateway > $WORKING_DIR/istio-gateway.yaml
    kubectl create cm -n $namespace keycloak-configmap --from-file=$WORKING_DIR/realm.json -o yaml --dry-run=client > $WORKING_DIR/keycloak-cm.yaml
    gomplate -d data=$WORKING_DIR/data.yaml -f ./keycloak/keycloak.yaml > $WORKING_DIR/keycloak.yaml

    #Create namespace and cert issuer for the customer
    echo "install Istio Ingress and Keycloak broker"
    apply_cluster   $WORKING_DIR/ca-issuer.yaml
    #Install Istio
    apply_cluster   $WORKING_DIR/istio-gateway.yaml
    #Install Keycloak cm
    apply_cluster   $WORKING_DIR/keycloak-cm.yaml
    #Install Keycloak
    apply_cluster   $WORKING_DIR/keycloak.yaml
    #install_oauth2
    generate_oauth2_data $name $namespace
    # Install oauth2-proxy for the customer
    gomplate -d data=$WORKING_DIR/data.yaml -f ./oath2-proxy/oauth2-proxy-template.yaml > $WORKING_DIR/oauth2-cfg-data.yaml
    helm template --namespace $namespace --values $WORKING_DIR/oauth2-cfg-data.yaml oauth2-proxy oauth2-proxy/oauth2-proxy > $WORKING_DIR/oauth2-proxy.yaml
    # Apply KNCC CR to update Istio Configmap for the newly installed oath2-proxy
    gomplate -d data=$WORKING_DIR/data.yaml -f ./oath2-proxy/configctrl.yaml > $WORKING_DIR/kncc-istio-cm.yaml

    echo "install_oauth2"
    #Install oauth2-proxy
    apply_cluster_namespace   $WORKING_DIR/oauth2-proxy.yaml $namespace

    #delete intermediate files
    rm $WORKING_DIR/realm.json
    rm $WORKING_DIR/oauth2-cfg-data.yaml
}

function create_istio {

    # Install Request Authentication
    gomplate -d data=$WORKING_DIR/data.yaml -f ./istio/request-auth-template.yaml > $WORKING_DIR/outer-istio.yaml
    # Install oauth configuration
    gomplate -d data=$WORKING_DIR/data.yaml -f ./istio/oauth-config-template.yaml >> $WORKING_DIR/outer-istio.yaml
    # Install outer gateway configuration for the customer
    gomplate -d data=$WORKING_DIR/data.yaml -f ./istio/outer-gateway-vs-template.yaml >> $WORKING_DIR/outer-istio.yaml
    gomplate -d data=$WORKING_DIR/data.yaml -f ./istio/keycloak-config-template.yaml >> $WORKING_DIR/outer-istio.yaml
    apply_cluster   $WORKING_DIR/kncc-istio-cm.yaml
    #Install Istio resources for the customer
    apply_cluster   $WORKING_DIR/outer-istio.yaml
}

function add_dns_entry {
    #To create new zone and record
    ipAddr=$(get_lb_ip)
    echo "===== creating dns entry for domain $domain1 ====="
    sudo pdnsutil create-zone $domain1
    sudo pdnsutil add-record $domain1 @ A $ipAddr
    sudo pdnsutil add-record $domain1 "*" CNAME $domain1

    echo "===== creating dns entry for domain $domain2 ====="
    sudo pdnsutil create-zone $domain2
    sudo pdnsutil add-record $domain2 @ A $ipAddr
    sudo pdnsutil add-record $domain2 "*" CNAME $domain2

}

function delete_packages {
    delete_cluster_namespace $WORKING_DIR/oauth2-proxy.yaml $namespace
    delete_cluster $WORKING_DIR/keycloak.yaml
    delete_cluster $WORKING_DIR/keycloak-cm.yaml
    delete_cluster $WORKING_DIR/istio-gateway.yaml
    delete_cluster $WORKING_DIR/ca-issuer.yaml
    delete_cluster $WORKING_DIR/calico.yaml

}
function delete_istio {
    delete_cluster $WORKING_DIR/outer-istio.yaml
    delete_cluster $WORKING_DIR/kncc-istio-cm.yaml
}

function delete_dns_entry {
    IFS=', ' read -r -a array <<< "$domain_list"
    domain1="${array[0]}"
    domain2="${array[1]}"
    echo "===== deleting dns entry for domain $domain1 ====="
    sudo pdnsutil delete-zone $domain1
    echo "===== deleting dns entry for domain $domain2 ====="
    sudo pdnsutil delete-zone $domain2
}

function add_gre_tunnel {
    gomplate -d data=$WORKING_DIR/data.yaml -f ./network/gre.yaml > $WORKING_DIR/gre.yaml
    apply_cluster $WORKING_DIR/gre.yaml
}

function delete_gre_tunnel {
    delete_cluster $WORKING_DIR/gre.yaml
}


name="oops"
namespace="oops"
# list of comma seprated values
domain_list="oops"
cidr="oops"

while getopts ":v:" flag
do
    case "${flag}" in
        v) values=${OPTARG}
           name=$(./yq eval '.name' $values)
           namespace=$(./yq eval '.namespace' $values)
           domain_list=$(./yq eval '.domain' $values)
           idp_server=$(./yq eval '.idp' $values)
           cidr=$(./yq eval '.cidr' $values)
           greTableName=$(./yq eval '.greTableName' $values)
           greServiceCIDR=$(./yq eval '.greServiceCIDR' $values);;
    esac
done

shift $((OPTIND-1))
WORKING_DIR=/tmp/$name
case "$1" in
     "create" )
        if [ "${name}" == "oops" ] ; then
            echo -e "ERROR - Customer name is required"
            exit
        fi
        if [ "${namespace}" == "oops"  ] ; then
            echo -e "Error - Namespace is required"
            exit
        fi
        if [ "${domain_list}" == "oops" ] ; then
            echo -e "Atleast one 1 domain name must be provided"
            exit
        fi

        if [ -d "$WORKING_DIR" ]; then rm -Rf $WORKING_DIR; fi
        mkdir -p $WORKING_DIR
        # Create Data file
        echo "Generate Data"
        generate_data
        echo "Create Packages"
        create_packages
        echo "Create Istio"
        create_istio
        sleep 10
        add_gre_tunnel
        echo "Customer $name resources created"
        echo "Adding DNS entry for customer $name"
        add_dns_entry
        echo "DNS entry created for customer $name"

        ## Clenup data files
        rm $WORKING_DIR/data.yaml
        ;;
    "delete" )
        delete_gre_tunnel
        delete_istio
        delete_packages
        echo "Deleting DNS entry for customer $name"
        delete_dns_entry
        echo "DNS entry deleted for customer $name"
        echo "Customer $name resources deleted"
    ;;
    *)
        usage ;;
esac

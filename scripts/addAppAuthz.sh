#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# Copyright (c) 2022 Intel Corporation

set -o errexit
set -o nounset
set -o pipefail

source _common.sh

name="oops"
namespace="oops"
# list of comma seprated values
app_name="oops"
role_name="oops"
url="oops"
role="oops"


function create_authz {
   cat << NET > $WORKING_DIR/$app_name-$role_name-role-data.yaml
namespace: $namespace
customerName: $name
appName: $app_name
rolename: $role_name
url: $url
role: $role
NET
    gomplate -d data=$WORKING_DIR/$app_name-$role_name-role-data.yaml -f ./istio/app-authz-template-url.yaml > $WORKING_DIR/$app_name-$role_name-istio.yaml

    # Apply app istio authorizations
    apply_cluster   $WORKING_DIR/$app_name-$role_name-istio.yaml
}

function delete_authz {
    delete_cluster $WORKING_DIR/$app_name-$role_name-istio.yaml
    rm $WORKING_DIR/$app_name-$role_name-istio.yaml
}


while getopts ":v:" flag
do
    case "${flag}" in
        v) values=${OPTARG}
           role_name=$(./yq eval '.rolename' $values)
           name=$(./yq eval '.name' $values)
           namespace=$(./yq eval '.namespace' $values)
           app_name=$(./yq eval '.app' $values)
           url=$(./yq eval '.url' $values)
           role=$(./yq eval '.role' $values);;
    esac
done
shift $((OPTIND-1))

WORKING_DIR=/tmp/$name
case "$1" in
    "create" )
        create_authz
        rm $WORKING_DIR/$app_name-$role_name-role-data.yaml
    ;;
    "delete" )
        delete_authz
    ;;
    *)
    usage ;;
esac
